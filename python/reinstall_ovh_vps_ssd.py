# -*- encoding: utf-8 -*-
import json
try:
    from ovh import ovh
except ImportError:
    # In case it's installed globally
    import ovh
import sys
import ovh_helper

# Credentials are stored in ~/.ovh.conf
# See ovh/README.rst
client = ovh.Client()

vps_list = client.get('/vps/')
if sys.argv[-1] in vps_list:
    vps = sys.argv[-1]
else:
    print("VPS not in list:")
    for vps in vps_list:
        print(vps)
    sys.exit(1)

current_distribution = client.get('/vps/{}/distribution'.format(vps))

available_templates = client.get('/vps/{}/templates'.format(vps))

def print_templates(client, vps, available_templates):
    for tid in available_templates:
        template = client.get('/vps/{}/templates/{}'.format(vps, tid))
        print("{}: {}".format(template["id"], template["distribution"]))


if "--get-state" in sys.argv:
    print(client.get('/vps/{}'.format(vps))["state"])
elif "--use-current" in sys.argv:
    if current_distribution['id'] in available_templates:
        print("Current template still available, using it")
        result = client.post('/vps/{}/reinstall'.format(vps), templateId=current_distribution['id'])
        print(result)
        ovh_helper.show_progress(client, vps, "reinstallVm")
    else:
        print("Current template no more available. Chose among:")
        print_templates(client, vps, available_templates)
elif sys.argv[-1] in available_templates:
    print("Chosen template available, using it")
    result = client.post('/vps/{}/reinstall'.format(vps), templateId=int(sys.argv[-1]))
    print(result)
    ovh_helper.show_progress(client, vps, "reinstallVm")
else:
    print("Chosen template not available. Chose among:")
    print_templates(client, vps, available_templates)

# Reboot in rescue:
# PUT /vps/{serviceName}
#   netbootMode "rescue" / "local"
#  changer son nom:
#   displayName: "..."  
