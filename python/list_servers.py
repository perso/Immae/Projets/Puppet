try:
    from ovh import ovh
except ImportError:
    # In case it's installed globally
    import ovh

client = ovh.Client()

print("OVH cloud instances:")
projects_list = client.get('/cloud/project/')
for project_id in projects_list:
    project = client.get('/cloud/project/{}'.format(project_id))
    print("\t{}:".format(project["description"]))
    instances_list = client.get('/cloud/project/{}/instance'.format(project_id))
    for instance in instances_list:
        print("\t\t{}: {}".format(instance["name"], instance["id"]))

vps_list = client.get('/vps/')

print("OVH VPS SSD servers:")
for vps in vps_list:
    print("\t{}".format(vps))

import hetzner_helper

print("Hetzner VPS servers:")
return_code, json = hetzner_helper.call("servers")
for server in json["servers"]:
    print("\t{}: {}".format(server["name"], server["id"]))
