# -*- encoding: utf-8 -*-
import json
try:
    from ovh import ovh
except ImportError:
    # In case it's installed globally
    import ovh
import sys
from ovh_helper import find_cloud_instance

infos = {}

# Credentials are stored in ~/.ovh.conf
# See ovh/README.rst
client = ovh.Client()

_, instance = find_cloud_instance(client, sys.argv[-1])

infos["ips"] = {}
for ip_infos in instance["ipAddresses"]:
    ip_infos["ipAddress"] = ip_infos.pop("ip")
    ip_infos["gateway"]   = ip_infos.pop("gatewayIp")

    if ip_infos["version"] == 4:
        infos["ips"]["v4"] = ip_infos
    else:
        infos["ips"]["v6"] = ip_infos
        infos["ips"]["v6"]["mask"] = 128

print(json.dumps(infos))
