# -*- encoding: utf-8 -*-
import json
try:
    from ovh import ovh
except ImportError:
    # In case it's installed globally
    import ovh
import sys
from ovh_helper import find_cloud_instance

# Credentials are stored in ~/.ovh.conf
# See ovh/README.rst
client = ovh.Client()

project, instance = find_cloud_instance(client, sys.argv[-1])

if "--rescue" in sys.argv:
    netboot_mode="rescue"
elif "--local" in sys.argv:
    netboot_mode="local"
else:
    netboot_mode=None

if netboot_mode is not None:
    result = client.post("/cloud/project/{}/instance/{}/rescueMode".format(project,
        instance["id"]), imageId=instance["imageId"], rescue=(netboot_mode == "rescue"))
    print(result)
else:
    result = client.post("/cloud/project/{}/instance/{}/reboot".format(project, instance["id"]), type="soft")
    print(result)

# reboot normal:
#result = client.post("/cloud/project/{}/instance/{}/reboot".format(project, instance["id"]), type="soft")
