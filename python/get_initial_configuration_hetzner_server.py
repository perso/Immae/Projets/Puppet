import sys
import json
import hetzner_helper

instance = sys.argv[-1]
instance = hetzner_helper.get("servers/{}".format(instance))[1]["server"]

infos = {}
infos["ips"] = {
        "v4": {
            "ipAddress": instance["public_net"]["ipv4"]["ip"],
            "gateway":   "172.31.1.1",
            },
        "v6": {
            "ipAddress": instance["public_net"]["ipv6"]["ip"].split("/")[0],
            "gateway":   "fe80::1",
            "mask":      instance["public_net"]["ipv6"]["ip"].split("/")[1],
            }
        }

print(json.dumps(infos))
