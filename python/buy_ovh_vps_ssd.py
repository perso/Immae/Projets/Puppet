# -*- encoding: utf-8 -*-
import json
try:
    from ovh import ovh
except ImportError:
    # In case it's installed globally
    import ovh
import sys
import ovh_helper

# Credentials are stored in ~/.ovh.conf
# See ovh/README.rst
client = ovh.Client()

# Create a cart
cart = client.post('/order/cart', ovhSubsidiary="FR")
cart_id = cart["cartId"]

# Assign it to my user
client.post('/order/cart/{}/assign'.format(cart_id))

# list of vps:
# client.get('/order/cart/{}/vps'.format(cart_id))
item = client.post('/order/cart/{}/vps'.format(cart_id),
        duration="P1M",
        planCode="vps_ssd_model1",
        pricingMode="default",
        quantity=1)
item_id = item["itemId"]

# Datacenter, OS, auto-renew
client.post('/order/cart/{}/item/{}/configuration'.format(cart_id, item_id),
        label="vps_ssd_datacenter",
        value="gra")
client.post('/order/cart/{}/item/{}/configuration'.format(cart_id, item_id),
        label="vps_ssd_os",
        value="linux--archlinux--64--en")
client.post('/order/cart/{}/item/{}/configuration'.format(cart_id, item_id),
        label="AUTO_RENEW_VPS",
        value=False)

#summary  = client.get('/order/cart/{}/summary'.format(cart_id))
#checkout = client.get('/order/cart/{}/checkout'.format(cart_id))

# Checkout
order = client.post('/order/cart/{}/checkout'.format(cart_id),
        waiveRetractationPeriod=True)
order_id = order["orderId"]
print(order_id)

payment_mean = client.get('/me/order/{}/availableRegisteredPaymentMean'.format(order_id))[0]['paymentMean']
payment_mean_id = client.get('/me/paymentMean/{}'.format(payment_mean))[0]

payment_mean_status = client.get('/me/paymentMean/{}/{}'.format(payment_mean, payment_mean_id))

if payment_mean_status["state"] != "valid":
    raise "Bouh"

paid_order = client.post('/me/order/{}/payWithRegisteredPaymentMean'.format(order_id),
        paymentMean=payment_mean, paymentMeanId=payment_mean_id)

if 'paymentDate' in paid_order:
    print("successful")
