# -*- encoding: utf-8 -*-
import json
try:
    from ovh import ovh
except ImportError:
    # In case it's installed globally
    import ovh
import sys
from ovh_helper import find_cloud_instance

# Credentials are stored in ~/.ovh.conf
# See ovh/README.rst
client = ovh.Client()

project, instance = find_cloud_instance(client, sys.argv[-1])

current_image = instance["imageId"]
available_images = client.get('/cloud/project/{}/image'.format(project),
        osType="linux",
        region=instance["region"])
available_images_ids = list(map(lambda x: x["id"], available_images))

def print_images(available_images):
    for image in available_images:
        print("{}: {}".format(image["name"], image["id"]))

def reinstall(image_id):
    return client.post('/cloud/project/{}/instance/{}/reinstall'.format(project, instance["id"]),
        imageId=image_id)

if "--get-state" in sys.argv:
    print(instance["status"])
elif "--use-current" in sys.argv:
    if current_image in available_images_ids:
        print("Current image still available, using it")
        print(reinstall(current_image))
    else:
        print("Current image no more available. Chose among:")
        print_images(available_images)
elif sys.argv[-1] in available_templates:
    print("Chosen image available, using it")
    print(reinstall(current_image))
else:
    print("Chosen image not available. Chose among:")
    print_images(available_images)

