# -*- encoding: utf-8 -*-
import json
try:
    from ovh import ovh
except ImportError:
    # In case it's installed globally
    import ovh
import sys

infos = {}

# Credentials are stored in ~/.ovh.conf
# See ovh/README.rst
client = ovh.Client()

vps_list = client.get('/vps/')
if sys.argv[-1] in vps_list:
    vps = sys.argv[-1]
else:
    print("VPS not in list:")
    for vps in vps_list:
        print(vps)
    sys.exit(1)

ips = client.get('/vps/{}/ips'.format(vps))

infos["ips"] = {}
for ip in ips:
    ip_infos = client.get('/vps/{}/ips/{}'.format(vps, ip))

    if ip_infos["version"] == "v4":
        infos["ips"]["v4"] = ip_infos
    else:
        infos["ips"]["v6"] = ip_infos
        infos["ips"]["v6"]["mask"] = 128

print(json.dumps(infos))
