# -*- encoding: utf-8 -*-
import json
try:
    from ovh import ovh
except ImportError:
    # In case it's installed globally
    import ovh
import sys
import ovh_helper

# Credentials are stored in ~/.ovh.conf
# See ovh/README.rst
client = ovh.Client()

vps_list = client.get('/vps/')
if sys.argv[-1] in vps_list:
    vps = sys.argv[-1]
else:
    print("VPS not in list:")
    for vps in vps_list:
        print(vps)
    sys.exit(1)

if "--rescue" in sys.argv:
    netboot_mode="rescue"
elif "--local" in sys.argv:
    netboot_mode="local"
else:
    netboot_mode=None

current_state=client.get("/vps/{}".format(vps))["netbootMode"]

if netboot_mode is None or current_state == netboot_mode:
    client.post("/vps/{}/reboot".format(vps))
    task_type="rebootVm"
else:
    client.put("/vps/{}".format(vps), netbootMode=netboot_mode)
    task_type="setNetboot"

ovh_helper.show_progress(client, vps, task_type)
