import sys
import hetzner_helper

instance = sys.argv[-1]
actions = []
if "--rescue" in sys.argv:
    actions.append("enable_rescue")
elif "--local" in sys.argv:
    actions.append("disable_rescue")

if "--hard" in sys.argv:
    actions.append("reset")
else:
    actions.append("reboot")

for action in actions:
    result = hetzner_helper.post("servers/{}/actions/{}".format(instance, action))
    print(result)
