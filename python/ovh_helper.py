import time
import sys

def show_progress(client, vps, task_type):
    running_task_id = client.get("/vps/{}/tasks?type={}".format(vps, task_type))[0]

    progress = 0
    state = "todo"
    print("  0 %", end='')
    while state != "done":
        old_progress = progress
        task = client.get("/vps/{}/tasks/{}".format(vps, running_task_id))
        progress = task["progress"]
        state = task["state"]

        if progress != old_progress:
            print("\r{:>3} %".format(progress), end="")
        time.sleep(3)

    print("\rFinished")

def find_cloud_instance(client, instance_id):
    projects_list = client.get('/cloud/project/')
    instances_list = []
    for project in projects_list:
        instances_list += list(map(lambda x: [project, x],
                client.get('/cloud/project/{}/instance'.format(project))))
    instances = dict(map(lambda x: (x[1]["id"], x), instances_list))
    if instance_id in instances:
        project_instance = instances[instance_id]
    else:
        print("Instance not in list:")
        for instance in instances_list:
            print("{}: {}".format(instance[1]["name"], instance[1]["id"]))
        sys.exit(1)
    return project_instance
