define etckeeper::run (
  $refreshonly = true,
  $reason = 'puppet run'
) {

  exec { "etckeeper::run::${name}":
    refreshonly => $refreshonly,
    command     => "/usr/bin/etckeeper commit '${reason}' || true",
  }
}
