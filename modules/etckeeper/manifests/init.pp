class etckeeper {
  ensure_packages('etckeeper')

  exec { 'etckeeper-gitignore':
    environment => ['VCS=git', 'LOWLEVEL_PACKAGE_MANAGER=pacman'],
    path        => '/usr/bin',
    command     => 'mkdir -p /tmp/etckeeper/.git && cd /tmp/etckeeper && /etc/etckeeper/update-ignore.d/01update-ignore && cp /tmp/etckeeper/.gitignore /etc/.gitignore_etckeeper && rm -r /tmp/etckeeper/',
    cwd         => '/tmp',
    creates     => '/etc/.gitignore_etckeeper'
  }

  file { 'etckeeper-additional_gitignore':
    source  => 'puppet:///modules/etckeeper/additional_gitignore',
    path    => '/etc/.gitignore_additional'
  }

  exec { 'etckeeper-concat_files':
    path        => '/usr/bin',
    command     => 'cat /etc/.gitignore_etckeeper /etc/.gitignore_additional > /etc/.gitignore',
    refreshonly => true,
    subscribe   => [Exec['etckeeper-gitignore'], File['etckeeper-additional_gitignore']]
  }

  exec { 'etckeeper-init':
    command => 'etckeeper init',
    path    => '/usr/bin',
    cwd     => '/etc',
    creates => '/etc/.git'
  }

}
