class base_installation::packages inherits base_installation {
  # Preinstalled
  ensure_packages(['base'])

  # Critical packages
  ensure_packages(['openssh', 'grub', 'sudo'])

  # Puppet dependencies
  ensure_packages(['git', 'puppet5'])

  # To run jobs
  ensure_packages(['at'])
}
