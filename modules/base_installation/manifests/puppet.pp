class base_installation::puppet (
  $password_seed = $base_installation::puppet_pass_seed
) inherits base_installation {
  File {
    mode  => "0600",
    owner => "root",
    group => "root",
  }

  exec { 'generate_password_seed':
    command     => "/usr/bin/openssl rand -base64 -out $password_seed 256",
    creates     => $password_seed,
    environment => "RANDFILE=/dev/null",
  }

  ### Until puppet fixes hist gettext-setup gem use
  package { 'gem:gettext-setup':
    name            => "gettext-setup",
    ensure          => present,
    provider        => "gem",
    install_options => "--no-user-install"
  }

  file { '/usr/lib/ruby/vendor_ruby/locales/':
    ensure => link,
    target => "/opt/puppetlabs/puppet/share/locale/",
  }
  ###

  file { '/usr/local/sbin/i_puppet_lock':
    mode   => "0755",
    ensure => present,
    source => "puppet:///modules/base_installation/scripts/puppet_lock"
  }

  file { '/usr/local/sbin/i_puppet_reset_and_apply':
    mode   => "0755",
    ensure => present,
    source => "puppet:///modules/base_installation/scripts/puppet_reset_and_apply"
  }

  file { '/usr/local/sbin/i_puppet_report_print':
    mode   => "0755",
    ensure => present,
    source => "puppet:///modules/base_installation/scripts/report_print.rb"
  }

  file { '/usr/local/sbin/puppet_apply':
    mode   => "0755",
    ensure => present,
    source => "puppet:///modules/base_installation/scripts/puppet_apply",
  }

  unless empty(find_file($password_seed)) {
    $ldap_password = generate_password(24, $password_seed, "ldap")
    $ssha_ldap_seed = generate_password(5, $password_seed, "ldap_seed")

    package { 'gem:ruby-ldap':
      name            => "ruby-ldap",
      ensure          => present,
      provider        => "gem",
      install_options => "--no-user-install"
    }

    package { 'gem:xmpp4r':
      name            => "xmpp4r",
      ensure          => present,
      provider        => "gem",
      install_options => "--no-user-install"
    }

    file { $password_seed:
      mode => "0600",
    }

    file { $base_installation::puppet_conf_path:
      ensure  => directory,
      require => [Package["puppet5"], Package["gem:xmpp4r"], Package["gem:ruby-ldap"]],
      recurse => true,
      purge   => true,
      force   => true,
    }

    $xmpp = lookup("base_installation::notify_xmpp", { "default_value" => {} })
    $slack = lookup("base_installation::notify_slack", { "default_value" => {} })

    file { "$base_installation::puppet_conf_path/puppet.conf":
      content => template("base_installation/puppet/puppet.conf.erb"),
      require => File[$base_installation::puppet_conf_path],
    }

    unless empty($xmpp) {
      file { "$base_installation::puppet_conf_path/xmpp.yaml":
        content => template("base_installation/puppet/xmpp.yaml.erb"),
        require => File[$base_installation::puppet_conf_path],
      }
    }

    unless empty($slack) {
      file { "$base_installation::puppet_conf_path/slack.yaml":
        content => template("base_installation/puppet/slack.yaml.erb"),
        require => File[$base_installation::puppet_conf_path],
      }
    }

    if file("$base_installation::puppet_notifies_path/host_ldap.info", "/dev/null") != "" and
      empty($facts["ldapvar"]) {
        fail("LDAP was activated but facts are not available")
    }

    file { $base_installation::puppet_notifies_path:
      ensure  => directory,
      require => [Package["puppet5"], Package["gem:xmpp4r"], Package["gem:ruby-ldap"]],
      recurse => true,
      purge   => true,
      force   => true,
    }

    $ips = lookup("ips", { 'default_value' => undef })
    concat { "$base_installation::puppet_notifies_path/host_ldap.info":
      ensure         => "present",
      mode           => "0600",
      require        => File[$base_installation::puppet_notifies_path],
      ensure_newline => true,
    }

    concat::fragment { "host_ldap add top":
      target  => "$base_installation::puppet_notifies_path/host_ldap.info",
      content => template("base_installation/puppet/host_ldap_add_top.info.erb"),
      order   => "00-01",
    }
    concat::fragment { "host_ldap add bottom":
      target  => "$base_installation::puppet_notifies_path/host_ldap.info",
      content => "EOF",
      order   => "00-99",
    }

    concat::fragment { "host_ldap mod top":
      target  => "$base_installation::puppet_notifies_path/host_ldap.info",
      content => template("base_installation/puppet/host_ldap_mod_top.info.erb"),
      order   => "01-01",
    }
    concat::fragment { "host_ldap mod bottom":
      target  => "$base_installation::puppet_notifies_path/host_ldap.info",
      content => "EOF",
      order   => "01-99",
    }
  }
}
