class base_installation::ssh inherits base_installation {
  class { 'ssh::server':
     storeconfigs_enabled => false,
     options              => {
        'AcceptEnv'                       => undef,
        'X11Forwarding'                   => 'yes',
        'PrintMotd'                       => 'no',
        'ChallengeResponseAuthentication' => 'no',
        'Subsystem'                       => 'sftp /usr/lib/openssh/sftp-server',
     },
     require              => Package["openssh"]
  }

  contain "ssh::server"
}
