class base_installation::system_config inherits base_installation {
  unless empty($base_installation::system_timezone) {
    file { "/etc/localtime":
      ensure => "link",
      target => "../usr/share/zoneinfo/$base_installation::system_timezone"
    }
  }

  if empty($base_installation::system_hostname) {
    $hostname = $base_installation::real_hostname
  } else {
    $hostname = $base_installation::system_hostname
  }

  file { '/etc/hostname':
    content => "$hostname\n",
  }

  exec { "set_hostname":
    command     => "/usr/bin/hostnamectl set-hostname $hostname",
    refreshonly => true,
    subscribe   => File["/etc/hostname"],
    returns     => [0, 1],
  }

}
