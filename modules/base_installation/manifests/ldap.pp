class base_installation::ldap inherits base_installation {
  ensure_packages(["openldap"])

  File {
    mode  => "0644",
    owner => "root",
    group => "root",
  }

  file { '/etc/openldap':
    ensure  => directory,
    require => Package["openldap"],
    recurse => true,
    purge   => true,
    force   => true,
  }

  file { '/etc/openldap/ldap.conf':
    ensure  => present,
    content => template("base_installation/ldap/ldap.conf.erb"),
    require => File['/etc/openldap'],
  }

  $password_seed  = lookup("base_installation::puppet_pass_seed")
  unless empty(find_file($password_seed)) {
    $ldap_server    = lookup("base_installation::ldap_server")
    $ldap_base      = lookup("base_installation::ldap_base")
    $ldap_dn        = lookup("base_installation::ldap_dn")
    $ldap_password  = generate_password(24, $password_seed, "ldap")
    $ldap_attribute = "uid"

    ensure_packages(["pam_ldap", "ruby-augeas"])
    file { "/etc/pam_ldap.conf":
      ensure  => "present",
      mode    => "0400",
      owner   => "root",
      group   => "root",
      content => template("base_installation/ldap/pam_ldap.conf.erb"),
    }

    ["system-auth", "passwd"].each |$service| {
      pam { "Allow to change ldap password via $service":
        ensure    => present,
        service   => $service,
        type      => "password",
        control   => "[success=done new_authtok_reqd=ok authinfo_unavail=ignore ignore=ignore default=bad]",
        module    => "pam_ldap.so",
        arguments => ["ignore_unknown_user", "ignore_authinfo_unavail"],
        position  => 'before *[type="password" and module="pam_unix.so"]',
        require   => Package["ruby-augeas"],
      }
    }

    ["system-auth", "su", "su-l"].each |$service| {
      ["auth", "account"].each |$type| {
        pam { "Allow $service to $type with ldap password":
          ensure    => present,
          service   => $service,
          type      => $type,
          control   => "[success=done new_authtok_reqd=ok authinfo_unavail=ignore ignore=ignore default=bad]",
          module    => "pam_ldap.so",
          arguments => ["ignore_unknown_user", "ignore_authinfo_unavail"],
          position  => "before *[type=\"$type\" and module=\"pam_unix.so\"]",
          require   => Package["ruby-augeas"],
        }
      }
    }
  }
}
