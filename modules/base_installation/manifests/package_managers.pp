class base_installation::package_managers inherits base_installation {
  file { '/etc/pacman.d/mirrorlist':
     ensure  => "present",
     path    => "/etc/pacman.d/mirrorlist",
     source  => 'puppet:///modules/base_installation/package_managers/mirrorlist',
     mode    => "0644",
     owner   => "root",
     group   => "root"
  }

  class { 'pacman':
    color     => true,
    usesyslog => true,
  }

  pacman::repo { 'multilib':
    order   => 15,
    include => '/etc/pacman.d/mirrorlist',
  }

  pacman::repo { 'immae':
    order    => 0,
    server   => 'https://release.immae.eu/packages/',
    siglevel => 'Optional',
  }

  exec { "refresh pacman":
    command     => "/usr/bin/pacman -Sy",
    refreshonly => true,
  }

  Concat["/etc/pacman.conf"] ~> Exec["refresh pacman"] -> Package <| name != "pacman" |>

  class { 'aur': }

  contain "pacman"
  contain "aur"
}
