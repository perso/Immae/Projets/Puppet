class base_installation::locales inherits base_installation {
  # Note: we don't care about other lines, they are not handled in
  # Puppet
  define locale_line ($locale, $charmap = 'UTF-8') {
    file_line { "/etc/locale.gen#$locale":
      path   => '/etc/locale.gen',
      line   => "$locale $charmap  ",
      match  => "#?$locale $charmap +$",
      notify => Exec["/usr/bin/locale-gen"],
    }
  }

  unless empty($base_installation::system_locales) {
    $base_installation::system_locales.each |$locale| {
      base_installation::locales::locale_line { "/etc/locale.gen#$locale":
        locale => $locale
      }
    }

    $main_locale = $base_installation::system_locales[0]
    exec { "set_main_locale":
      command => "/usr/bin/systemd-firstboot --locale=$main_locale",
      creates => "/etc/locale.conf",
    }
  }

  exec { '/usr/bin/locale-gen':
    refreshonly => true,
  }


  file { "/etc/vconsole.conf":
    ensure => "link",
    target => "/dev/null",
  }

}
