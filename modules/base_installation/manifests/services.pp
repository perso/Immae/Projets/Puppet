class base_installation::services inherits base_installation {

  if $facts["in_chroot"] == undef or empty($facts["in_chroot"]) {
    $ensure = "running"
  } else {
    $ensure = undef
  }

  service { "sshd":
    ensure  => $ensure,
    enable  => true,
    require => Package["openssh"],
  }

  service { "atd":
    ensure  => $ensure,
    enable  => true,
    require => Package["at"],
  }

  service { "systemd-networkd":
    ensure => $ensure,
    enable => true,
  }

  service { "systemd-resolved":
    ensure => $ensure,
    enable => true,
  }

  service { "systemd-timesyncd":
    ensure => $ensure,
    enable  => true
  }

  service { "cronie":
    ensure  => $ensure,
    enable  => true,
    require => Package["cronie"],
  }

  file { '/etc/systemd/system/getty@tty1.service.d/':
    ensure => "directory",
    path   => "/etc/systemd/system/getty@tty1.service.d/",
    mode   => "0755",
    owner  => "root",
    group  => "root"
  }

  file { '/etc/systemd/system/getty@tty1.service.d/noclear.conf':
     ensure  => "present",
     path    => "/etc/systemd/system/getty@tty1.service.d/noclear.conf",
     source  => 'puppet:///modules/base_installation/services/getty_conf_override.conf',
     recurse =>  true,
     mode    => "0644",
     owner   => "root",
     group   => "root"
  }

  $ip6 = lookup("ips.v6", { 'default_value' => undef })
  file { '/etc/systemd/network/en-dhcp.network':
    ensure  => "present",
    path    => "/etc/systemd/network/en-dhcp.network",
    content => template('base_installation/services/en-dhcp.network.erb'),
    mode    => "0644",
    owner   => "root",
    group   => "root"
  }

}
