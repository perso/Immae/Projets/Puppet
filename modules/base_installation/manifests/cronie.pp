class base_installation::cronie inherits base_installation {
  ensure_packages(['cronie'])

  Package["cronie"] -> Cron::Job <| |>
  Package["cronie"] -> Cron::Job::Multiple <| |>
  Package["cronie"] -> Cron::Daily <| |>

  unless empty($base_installation::puppet_code_path) {
    file { 'post-hook':
      ensure  => file,
      path    => "$base_installation::puppet_code_path/.git/hooks/post-merge",
      source  => 'puppet:///modules/base_installation/cronie/puppet-post-merge',
      mode    => '0755',
      owner   => root,
      group   => root,
    }
    cron::job { 'puppet-pull-apply':
      ensure      => present,
      environment => [ 'MAILTO=""' ],
      command     => "cd $base_installation::puppet_code_path ; /usr/bin/git pull",
      user        => root,
      minute      => '15,45',
      require     => File['post-hook'],
      description => "Pull git and apply puppet changes",
    }
    cron::job::multiple { 'puppet-apply':
      ensure => present,
      environment => [ 'MAILTO=""' ],
      jobs   => [
        {
          command     => "cd $base_installation::puppet_code_path ; /usr/local/sbin/puppet_apply $base_installation::puppet_code_path/manifests/site.pp",
          user        => root,
          minute      => '*/20',
          description => "Re-apply puppet changes regularly",
        },
        {
          command     => "cd $base_installation::puppet_code_path ; /usr/local/sbin/puppet_apply $base_installation::puppet_code_path/manifests/site.pp",
          user        => root,
          special     => "reboot",
          description => "Re-apply puppet changes at reboot",
        }
      ]
    }

    cron::daily { 'pacman_keys':
      ensure      => present,
      command     => "pacman-key --populate archlinux",
      user        => root,
      minute      => "0",
      hour        => "1",
      description => "Populate pacman keys",
    }
  }
}
