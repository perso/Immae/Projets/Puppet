class base_installation (
  Optional[String]        $grub_device          = $base_installation::params::grub_device,
  Optional[String]        $ldap_base            = $base_installation::params::ldap_base,
  Optional[String]        $ldap_cert_path       = $base_installation::params::ldap_cert_path,
  Optional[String]        $ldap_cn              = $base_installation::params::ldap_cn,
  Optional[String]        $ldap_dn              = $base_installation::params::ldap_dn,
  Optional[String]        $ldap_server          = $base_installation::params::ldap_server,
  Optional[String]        $ldap_uri             = $base_installation::params::ldap_uri,
  Optional[String]        $puppet_code_path     = $base_installation::params::puppet_code_path,
  Optional[String]        $puppet_conf_path     = $base_installation::params::puppet_conf_path,
  Optional[String]        $puppet_notifies_path = $base_installation::params::puppet_notifies_path,
  Optional[String]        $puppet_pass_seed     = $base_installation::params::puppet_pass_seed,
  Optional[String]        $puppet_ssl_path      = $base_installation::params::puppet_ssl_path,
  Optional[String]        $real_hostname        = $base_installation::params::real_hostname,
  Optional[String]        $system_hostname      = $base_installation::params::system_hostname,
  Optional[Array[String]] $system_locales       = $base_installation::params::system_locales,
  Optional[String]        $system_timezone      = $base_installation::params::system_timezone,
  Optional[Array[Hash]]   $system_users         = $base_installation::params::system_users,
) inherits base_installation::params {
  contain ::base_installation::packages
  contain ::base_installation::locales
  contain ::base_installation::system_config
  contain ::base_installation::kernel_modules
  contain ::base_installation::grub
  contain ::base_installation::firewall
  contain ::base_installation::logs
  contain ::base_installation::cronie
  contain ::base_installation::ssh
  contain ::base_installation::ldap
  contain ::base_installation::services
  contain ::base_installation::users
  contain ::base_installation::package_managers
  contain ::base_installation::puppet
  contain ::base_installation::cleanup
}
