class base_installation::grub inherits base_installation {
  ensure_packages(['grub'])

  # unless empty($base_installation::grub_device) {
  #   exec { 'install GRUB':
  #     command   => "/usr/bin/grub-install --target=i386-pc $base_installation::device",
  #     subscribe => Package["grub"],
  #   }
  # }

  file_line { "/etc/default/grub#GRUB_CMDLINE_LINUX":
    path    => "/etc/default/grub",
    line    => 'GRUB_CMDLINE_LINUX=" console=tty0 console=ttyS0,115200"',
    match   => '^GRUB_CMDLINE_LINUX=',
    require => Package["grub"],
  }

  # exec { 'update GRUB config':
  #   command     => "/usr/bin/grub-mkconfig -o /boot/grub/grub.cfg",
  #   refreshonly => true
  # }
}
