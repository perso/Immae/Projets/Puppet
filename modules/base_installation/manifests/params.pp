class base_installation::params {
  $puppet_code_path     = "/etc/puppetlabs/code"
  $puppet_conf_path     = "/etc/puppetlabs/puppet"
  $puppet_notifies_path = "/etc/puppetlabs/notifies"
  $puppet_pass_seed     = "/etc/puppetlabs/puppet/password_seed"
  $puppet_ssl_path      = "/etc/puppetlabs/ssl"
  $grub_device          = "/dev/sda"
  $ldap_base            = "dc=example,dc=com"
  $ldap_cn              = "node"
  $ldap_dn              = "cn=node,ou=hosts,dc=example,dc=com"
  $ldap_cert_path       = "/etc/ssl/certs/ca-certificates.crt"
  $ldap_uri             = "ldaps://ldap.example.com"
  $ldap_server          = "ldap.example.com"
  $real_hostname        = "example.com"
  $system_hostname      = "example.com"
  $system_locales       = ["en_US.UTF-8"]
  $system_timezone      = "UTC"
  $system_users         = [
    {
      userid   => 1000,
      username => "example",
      groups   => ["sudo"],
      keys     => [
        {
          host     => "local.example.com",
          key      => "AAAAB3",
          key_type => "ssh-rsa"
        }
      ]
    }
  ]
}
