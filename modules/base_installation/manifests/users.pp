class base_installation::users (
  $users = $base_installation::system_users,
) inherits base_installation {
  ensure_packages('ruby-shadow')
  user { 'root':
    password       => '!',
    purge_ssh_keys => ["/root/.ssh/authorized_keys"],
  }

  class { 'sudo':
    config_file_replace => false,
    # Missing in the sudo package, should no be mandatory
    package_ldap        => false
  }

  sudo::conf { 'wheel':
    priority => 10,
    content  => "%wheel ALL=(ALL) ALL",
    require  => Package["sudo"],
  }

  contain "sudo"

  $users.each |$user| {
    if ($user["username"] != "root") {
      unless $user["shell"] == undef or empty($user["shell"]) {
        ensure_packages([$user["shell"]])
        $shell = "/bin/${user[shell]}"
      } else {
        $shell = undef
      }

      user { "${user[username]}:${user[userid]}":
        name           => $user[username],
        uid            => $user[userid],
        ensure         => "present",
        groups         => $user[groups],
        managehome     => true,
        system         => !!$user[system],
        home           => "/home/${user[username]}",
        shell          => $shell,
        notify         => Exec["remove_password:${user[username]}:${user[userid]}"],
        purge_ssh_keys => true
      }

      exec { "remove_password:${user[username]}:${user[userid]}":
        command     => "/usr/bin/chage -d 0 ${user[username]} && /usr/bin/passwd -d ${user[username]}",
        onlyif      => "/usr/bin/test -z '${user[password]}'",
        refreshonly => true
      }
    }

    if has_key($user, "keys") {
      $user[keys].each |$key| {
        if has_key($key, "command") {
          ssh_authorized_key { "${user[username]}@${key[host]}":
            name    => "${user[username]}@${key[host]}",
            user    => $user[username],
            type    => $key[key_type],
            key     => $key[key],
            options => [
              "command=\"${key[command]}\"",
              "no-port-forwarding",
              "no-X11-forwarding",
              "no-pty",
            ],
          }
        } else {
          ssh_authorized_key { "${user[username]}@${key[host]}":
            name => "${user[username]}@${key[host]}",
            user => $user[username],
            type => $key[key_type],
            key  => $key[key],
          }
        }
      }
    }
  }

}
