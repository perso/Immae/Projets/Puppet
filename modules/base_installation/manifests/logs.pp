class base_installation::logs inherits base_installation {
  class { '::logrotate':
    manage_cron_daily => false,
    config => {
      rotate_every => 'week',
      rotate       => 4,
      create       => true,
      compress     => true,
      olddir       => '/var/log/old',
      tabooext     => "+ .pacorig .pacnew .pacsave",
    }
  }

  logrotate::rule { 'wtmp':
    path         => '/var/log/wtmp',
    rotate_every => 'month',
    create       => true,
    create_mode  => '0664',
    create_owner => 'root',
    create_group => 'utmp',
    rotate       => 1,
    minsize      => '1M',
  }
  logrotate::rule { 'btmp':
    path         => '/var/log/btmp',
    missingok    => true,
    rotate_every => 'month',
    create       => true,
    create_mode  => '0600',
    create_owner => 'root',
    create_group => 'utmp',
    rotate       => 1,
  }

  contain "::logrotate"
}
