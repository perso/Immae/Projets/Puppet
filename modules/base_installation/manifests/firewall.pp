class base_installation::firewall inherits base_installation {
  ensure_packages(["whois"], { 'install_options' => '--asdeps' })

  class { 'fail2ban':
    logtarget => 'SYSLOG',
    backend   => 'systemd'
  }

  fail2ban::jail { 'sshd':
    backend  => 'systemd',
    port     => 'ssh',
    filter   => 'sshd',
    maxretry => 10,
    bantime  => 86400,
    logpath  => '',
    order    => 10
  }

  contain "fail2ban"
}
