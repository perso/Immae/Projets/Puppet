class base_installation::kernel_modules inherits base_installation {
  file { '/etc/modprobe.d/pcspkr_no_autoload.conf':
     ensure => "present",
     path   => "/etc/modprobe.d/pcspkr_no_autoload.conf",
     source => 'puppet:///modules/base_installation/kernel_modules/pcspkr_no_autoload.conf',
     mode   => "0644",
     owner  => "root",
     group  => "root"
  }

}
