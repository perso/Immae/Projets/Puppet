#
# Simple module for logging messages on the client-side


module Puppet
  Type.newtype(:notify_refresh) do
    @doc = "Sends an arbitrary message to the agent run-time log."

    apply_to_all

    newproperty(:message, :idempotent => false) do
      desc "The message to be sent to the log."
      def sync(refreshing = false)
        if refreshing || !@resource.refreshonly?
          Puppet.send(@resource[:loglevel], self.should)
        end
        return
      end

      def retrieve
        :absent
      end

      def insync?(is)
        true
      end

      defaultto { @resource[:name] }
    end

    def refresh
      self.property(:message).sync(true)
    end

    newparam(:name) do
      desc "An arbitrary tag for your own reference; the name of the message."
      isnamevar
    end

    newparam(:refreshonly, :boolean => true, :parent => Puppet::Parameter::Boolean) do
      defaultto false
    end

  end
end
