module Puppet::Parser::Functions
  newfunction(:find_host, :type => :rvalue, :doc => <<-EOS
This function searches through the hosts to find the one with matching cn
    EOS
  ) do |arguments|

    if (arguments.size != 2) then
      raise(Puppet::ParseError, "grep(): Wrong number of arguments given #{arguments.size} for 2")
    end

    hosts = arguments[0]
    cn = arguments[1]

    hosts.find { |host| host["cn"].any? { |cn_| cn_ == cn } }
  end
end

# vim: set ts=2 sw=2 et :
