require 'puppet'

Puppet::Reports.register_report(:cat_files) do
  FOLLOWED_RESOURCES = [
    "File[/etc/puppetlabs/notifies/host_ldap.info]",
  ]

  def process
    self.resource_statuses.each do |name, status|
      if FOLLOWED_RESOURCES.include?(status.resource) && status.events.any? { |e| e.status == "success" }
        puts File.open(status.title, "r").read()
      end
    end
  end

end

