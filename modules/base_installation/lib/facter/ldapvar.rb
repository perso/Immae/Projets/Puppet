begin
  require 'ldap'
  require 'puppet/util/ldap/connection'

  Facter.add("ldapvar") do
    setcode do
      if Puppet[:node_terminus].to_sym != :ldap
        data = []
      else
        begin
          conn = Puppet::Util::Ldap::Connection.instance
          conn.start
          connection = conn.connection
        rescue ::LDAP::ResultError => e
          raise Puppet::ParseError, ("ldapquery(): LDAP ResultError - #{e.message}")
        end

        host = Facter.value('ec2_metadata')["hostname"]
        base = Puppet[:ldapbase]
        scope  = ::LDAP::LDAP_SCOPE_SUBTREE
        filter = "(objectclass=*)"

        data = {
          :self  => {},
          :other => [],
        }

        connection.search(base, scope, filter) do |entry|
          data_ = entry.to_hash
          if data_["objectClass"].any? { |class_| class_ == "puppetClient" }
            data_['vars'] = (data_[Puppet[:ldapstackedattrs]] || [])
              .map { |var| var.split("=", 2) }
              .group_by { |(key, value)| key }
              .map { |key, value| [key, value.map(&:last)] }
              .to_h

            data[:other] << data_

            if data_["cn"].any? { |cn| cn == host }
              data[:self] = data_
            end
          end
        end

        data
      end
    end
  end
rescue LoadError
  # No facts
end
