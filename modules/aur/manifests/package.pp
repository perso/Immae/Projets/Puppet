define aur::package (
  $ensure = 'present',
) {

  case $ensure {
    'present': {
      exec { "pacman::aur::install::${name}":
        cwd       => "/tmp",
        require   => Class[aur::aura],
        command   => "/usr/bin/sudo /usr/bin/aura --noconfirm -A ${name}",
        user      => "aur-builder",
        unless    => "/usr/bin/aura -Qk ${name}",
        logoutput => 'on_failure',
        timeout   => 1800,
      }
    }
    'absent': {
      exec { "pacman::aur::remove::${name}":
        cwd       => "/tmp",
        require   => Class[aur::aura],
        command   => "/usr/bin/sudo /usr/bin/aura --noconfirm -Rs ${name}",
        user      => "aur-builder",
        onlyif    => "/usr/bin/aura -Qi ${name}",
        logoutput => 'on_failure',
      }
    }
    default: {
      fail("Pacman::Aur[${name}] ensure parameter must be either 'present' or 'absent'")
    }

  }
}
