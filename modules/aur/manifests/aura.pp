class aur::aura(
) {
  user { "aur-builder":
    name   => "aur-builder",
    system => true,
    ensure => "present"
  }

  sudo::conf { 'aur-builder':
    content => "aur-builder ALL=(root) NOPASSWD: /usr/bin/aura *"
  }

  ensure_packages(['base-devel'], { 'install_options'  => '--needed' })
  ensure_packages(['gmp', 'pcre'], { 'install_options' => '--asdeps' })

  # aur.sh seems down
  #$aur_url = "https://aur.sh"
  #$aur_url = "https://raw.githubusercontent.com/stuartpb/aur.sh/dc2f3fcaa90a4d3d1e8d746160559b5d3123a425/aur.sh"

  exec { 'aur::aura':
    cwd       => "/tmp",
    path      => "/usr/bin",
    command   => "curl https://aur.archlinux.org/cgit/aur.git/snapshot/aura-bin.tar.gz | tar xz && cd aura-bin && makepkg aura-bin && mv /tmp/aura-bin/aura-bin-*-x86_64.pkg.tar.xz /tmp/aura-bin-x86_64.pkg.tar.xz && rm -rf /tmp/aura-bin",
    user      => "aur-builder",
    unless    => '/usr/bin/pacman -Qo aura',
    require   => Package['base-devel'],
    logoutput => 'on_failure',
  }

  package { 'aura-bin':
    ensure  => "present",
    source  => "/tmp/aura-bin-x86_64.pkg.tar.xz",
    require => Exec['aur::aura'],
    notify  => Exec['aur::aura::cleanup']
  }

  exec { 'aur::aura::cleanup':
    path        => "/usr/bin/",
    command     => "rm -f /tmp/aura-bin-x86_64.pkg.tar.xz",
    refreshonly => true
  }
}
