class profile::postgresql (
  Optional[String] $pg_user  = "postgres",
  Optional[String] $pg_group = "postgres",
) {
  $password_seed = lookup("base_installation::puppet_pass_seed")

  class { '::postgresql::globals':
    encoding             => 'UTF-8',
    locale               => 'en_US.UTF-8',
    pg_hba_conf_defaults => false,
  }

  class { '::postgresql::client': }

  # FIXME: postgresql module is buggy and doesn't create dir?
  file { "/var/lib/postgres":
    ensure  => directory,
    owner   => $pg_user,
    group   => $pg_group,
    before  => File["/var/lib/postgres/data"],
    require => Package["postgresql-server"],
  }

  class { '::postgresql::server':
    postgres_password => generate_password(24, $password_seed, "postgres"),
    listen_addresses  => "*",
  }

  profile::postgresql::base_pg_hba_rules { "default": }

  @profile::monitoring::local_service { "Databases are present in postgresql":
    sudos => {
      "naemon-postgresql-database-public" => "naemon ALL=(postgres) NOPASSWD: /usr/bin/psql -c select\\ nspname\\ from\\ pg_catalog.pg_namespace"
    },
    local => {
      check_command => "check_command_output!psql -c 'select nspname from pg_catalog.pg_namespace'!public!-r postgres",
    }
  }
}

