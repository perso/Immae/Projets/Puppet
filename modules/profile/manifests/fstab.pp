class profile::fstab (
  Optional[Array] $mounts = []
) {
  $mounts.each |$mount| {
    ensure_packages(["parted"])

    unless empty($mount) {
      $infos = split($mount, ';')

      file { $infos[0]:
        ensure => directory,
      } ->
      mount { $infos[0]:
        ensure => mounted,
        device => "UUID=${infos[1]}",
        fstype => $infos[2]
      }

      @profile::monitoring::local_service { "Size on ${infos[0]} partition":
        local => {
          check_command => "check_local_disk!10%!5%!${infos[0]}",
        };
      }
    }
  }
}
