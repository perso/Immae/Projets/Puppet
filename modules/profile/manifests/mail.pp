class profile::mail (
  String            $mailhub,
  Optional[Integer] $mailhub_port = 25,
  Optional[String]  $default_target = "postmaster",
) {
  ensure_packages(["s-nail", "ssmtp"])

  $mail_host = "mail.immae.eu"
  $password_seed = lookup("base_installation::puppet_pass_seed")
  $ldap_password = generate_password(24, $password_seed, "ldap")
  $ldap_cn = lookup("base_installation::ldap_cn")

  file { "/etc/ssmtp/ssmtp.conf":
    ensure  => "present",
    content =>  template("profile/mail/ssmtp.conf.erb"),
  }
}

