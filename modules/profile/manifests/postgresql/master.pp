define profile::postgresql::master (
  $letsencrypt_host          = undef,
  $backup_hosts              = [],
  Optional[String] $pg_user  = "postgres",
  Optional[String] $pg_group = "postgres",
) {
  $pg_path = "/var/lib/postgres"
  $pg_data_path = "$pg_path/data"

  $postgresql_backup_port = $facts.dig("ldapvar", "self", "vars", "postgresql_backup_port", 0)
  if ($postgresql_backup_port and !empty($backup_hosts)) {
    $password_seed = lookup("base_installation::puppet_pass_seed")
    $ldap_cn = lookup("base_installation::ldap_cn")
    $ldap_password = generate_password(24, $password_seed, "ldap")

    $host = find_host($facts["ldapvar"]["other"], $backup_hosts[0])
    if empty($host) {
      fail("No backup host to recover from")
    } elsif has_key($host["vars"], "host") {
      $pg_backup_host = $host["vars"]["host"][0]
    } else {
      $pg_backup_host = $host["vars"]["real_hostname"][0]
    }

    exec { "pg_basebackup $pg_data_path":
      cwd         => $pg_path,
      user        => $pg_user,
      creates     => "$pg_data_path/PG_VERSION",
      environment => ["PGPASSWORD=$ldap_password"],
      command     => "/usr/bin/pg_basebackup -w -h $pg_backup_host -p $postgresql_backup_port -U $ldap_cn -D $pg_data_path",
      before      => File[$pg_data_path],
      require     => File[$pg_path],
      notify      => Exec["cleanup pg_basebackup $pg_data_path"],
    } -> file { "$pg_data_path/recovery.conf":
      before => Concat["$pg_data_path/pg_hba.conf"],
      ensure => absent,
    }

    exec { "cleanup pg_basebackup $pg_data_path":
      refreshonly => true,
      cwd         => $pg_path,
      user        => $pg_user,
      before      => Class["postgresql::server::config"],
      command     => "/usr/bin/rm -f $pg_data_path/postgresql.conf && touch $pg_data_path/postgresql.conf",
    }
  }

  profile::postgresql::ssl { $pg_data_path:
    cert                => "/etc/letsencrypt/live/$letsencrypt_host/cert.pem",
    key                 => "/etc/letsencrypt/live/$letsencrypt_host/privkey.pem",
    require             => Letsencrypt::Certonly[$letsencrypt_host],
    handle_config_entry => true,
  }

  $backup_hosts.each |$backup_host| {
    profile::postgresql::replication { $backup_host:
      handle_config => true,
      handle_role   => true,
      handle_slot   => true,
      add_self_role => true,
    }

    @profile::monitoring::local_service { "Postgresql replication for $backup_host is up to date":
      sudos => {
        "naemon-postgresql-replication-$backup_host" => "naemon ALL=(postgres) NOPASSWD: /etc/naemon/monitoring-plugins/check_postgres_replication $backup_host /run/postgresql 5432"

      },
      local => {
        check_command => "check_postgresql_replication!$backup_host!/run/postgresql!5432",
      }
    }
  }
}
