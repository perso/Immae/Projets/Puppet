define profile::postgresql::backup_pgbouncer (
  String $base_path,
  Hash   $pg_infos,
  String $pg_user  = "postgres",
  String $pg_group = "postgres",
) {
  include "profile::postgresql::pam_ldap_pgbouncer"
  ensure_packages(["pgbouncer"])

  $host_cn = $title

  $host = find_host($facts["ldapvar"]["other"], $host_cn)
  if empty($host) {
    fail("No host found for pgbouncer")
  } elsif has_key($host["vars"], "host") {
    $pg_backup_host = $host["vars"]["host"][0]
  } else {
    $pg_backup_host = $host["vars"]["real_hostname"][0]
  }

  $pg_path = "$base_path/$pg_backup_host/postgresql"

  if has_key($host["vars"], "postgresql_backup_port") {
    $pg_port = " port=${host[vars][postgresql_backup_port][0]}"
  } else {
    $pg_port = ""
  }

  # Config
  ensure_resource("concat", "/etc/pgbouncer/pgbouncer.ini", {
    mode           => "0644",
    owner          => "root",
    group          => "root",
    ensure_newline => true,
    notify         => Service["pgbouncer"],
    before         => Service["pgbouncer"],
  })

  ensure_resource("concat::fragment", "pgbouncer_head", {
    target => "/etc/pgbouncer/pgbouncer.ini",
    order  => 01,
    source => "puppet:///modules/profile/postgresql/pgbouncer_head.ini",
  })

  concat::fragment { "pgbouncer_$pg_backup_host":
    target  => "/etc/pgbouncer/pgbouncer.ini",
    order   => 02,
    content => "${pg_infos[pgbouncer_dbname]} = host=$pg_path$pg_port user=${pg_infos[dbuser]} dbname=${pg_infos[dbname]}",
  }

  # Current pam configuration requires password for postgres
  # @profile::monitoring::local_service { "Database ${pg_infos[pgbouncer_dbname]} is available in pgbouncer":
  #   sudos => {
  #     "naemon-postgresql-database-public" => "naemon ALL=(postgres) NOPASSWD: /usr/bin/psql -c select\ nspname\ from\ pg_catalog.pg_namespace ${pg_infos[pgbouncer_dbname]}"
  #   },
  #   local => {
  #     check_command => "check_command_output!psql -c 'select nspname from pg_catalog.pg_namespace' ${pg_infos[pgbouncer_dbname]}!public!-r postgres",
  #   }
  # }

  # pg_hba for accessed cluster
  postgresql::server::pg_hba_rule { "$pg_backup_host - local access as ${pg_infos[dbuser]} user":
    description => "Allow local access to ${pg_infos[dbuser]} user",
    type        => 'local',
    database    => $pg_infos["dbname"],
    user        => $pg_infos["dbuser"],
    auth_method => 'trust',
    order       => "01-00",
    target      => "$pg_path/pg_hba.conf",
    postgresql_version => "10",
  }

  # service
  ensure_resource("file", "/etc/systemd/system/pgbouncer.service.d", {
    ensure => "directory",
    mode   => "0644",
    owner  => "root",
    group  => "root",
  })

  ensure_resource("file", "/etc/systemd/system/pgbouncer.service.d/override.conf", {
    ensure  => "present",
    mode    => "0644",
    owner   => "root",
    group   => "root",
    content => "[Service]\nUser=\nUser=$pg_user\n",
    notify  => Service["pgbouncer"],
    before  => Service["pgbouncer"],
  })

  ensure_resource("service", "pgbouncer", {
    ensure  => "running",
    enable  => true,
    require => [
      Package["pgbouncer"],
      File["/etc/systemd/system/pgbouncer.service.d/override.conf"],
      Concat["/etc/pgbouncer/pgbouncer.ini"]
    ],
  })


}
