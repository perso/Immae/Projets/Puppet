define profile::postgresql::backup_dump (
  String                             $pg_user  = "postgres",
  String                             $pg_group = "postgres",
  Optional[Variant[String, Integer]] $pg_port  = undef,
) {
  $base_path        = $title
  $pg_path          = "$base_path/postgresql"
  $pg_backup_path   = "$base_path/postgresql_backup"
  $pg_host          = split($base_path, "/")[-1]

  ensure_packages(["python", "python-pip"])
  ensure_resource("package", "pylog2rotate", {
    source   => "git+https://github.com/avian2/pylog2rotate",
    ensure   => present,
    provider => "pip3",
    require  => Package["python-pip"],
  })

  file { $pg_backup_path:
    ensure  => directory,
    owner   => $pg_user,
    group   => $pg_group,
    mode    => "0700",
    require => File[$base_path],
  }

  if $pg_port and !empty($pg_port) {
    $pg_port_arg = " -p $pg_port"
  } else {
    $pg_port_arg = ""
  }

  file { "/usr/local/sbin/backup_psql_$pg_host.sh":
    mode    => "0755",
    content => template("profile/postgresql/backup_psql.sh.erb"),
  }

  if ($pg_host == "eldiron.immae.eu") {
    cron::job::multiple { "backup_psql_$pg_host":
      ensure  => "present",
      require => [File[$pg_backup_path], File[$pg_path]],
      jobs    => [
        {
          command     => "/usr/local/sbin/backup_psql_$pg_host.sh",
          user        => $pg_user,
          hour        => "22,4,10,16",
          minute      => 0,
          description => "Backup the database",
        },
        {
          command     => "/usr/bin/rm -f $(ls -1 $pg_backup_path/*.sql | sort -r | sed -e '1,12d')",
          user        => $pg_user,
          hour        => 3,
          minute      => 0,
          description => "Cleanup the database backups",
        },
      ]
    }
  } else {
    cron::job::multiple { "backup_psql_$pg_host":
      ensure  => "present",
      require => [File[$pg_backup_path], File[$pg_path]],
      jobs    => [
        {
          command     => "/usr/local/sbin/backup_psql_$pg_host.sh",
          user        => $pg_user,
          hour        => "22,4,10,16",
          minute      => 0,
          description => "Backup the database",
        },
        {
          command     => "/usr/bin/rm -f $(ls -1 $pg_backup_path/*.sql | grep -v 'T22:' | sort -r | sed -e '1,12d')",
          user        => $pg_user,
          hour        => 3,
          minute      => 0,
          description => "Cleanup the database backups",
        },
        {
          command     => "cd $pg_backup_path ; /usr/bin/rm -f $(ls -1 *T22*.sql | log2rotate --skip 7 --fuzz 7 --delete --format='\%Y-\%m-\%dT\%H:\%M:\%S+02:00.sql')",
          user        => $pg_user,
          hour        => 3,
          minute      => 1,
          description => "Cleanup the database backups exponentially",
        },
      ]
    }
  }

  @profile::monitoring::local_service { "Last postgresql dump in $pg_backup_path is not too old":
    sudos => {
      "naemon-postgresql-dumps-$pg_host" => "naemon  ALL=($pg_user) NOPASSWD: /usr/bin/find $pg_backup_path -mindepth 1 -maxdepth 1 -printf %T@?n",
    },
    local => {
      check_command => "check_last_file_date!$pg_backup_path!7!$pg_user",
    }
  }
}
