define profile::postgresql::ssl (
  Optional[String]  $cert                 = undef,
  Optional[String]  $key                  = undef,
  Optional[String]  $certname             = undef,
  Optional[Boolean] $copy_keys            = true,
  Optional[Boolean] $handle_config_entry  = false,
  Optional[Boolean] $handle_concat_config = false,
  Optional[String]  $pg_user              = "postgres",
  Optional[String]  $pg_group             = "postgres",
) {
  $datadir = $title

  file { "$datadir/certs":
    ensure  => directory,
    mode    => "0700",
    owner   => $pg_user,
    group   => $pg_group,
    require => File[$datadir],
  }

  if empty($cert) or empty($key) {
    if empty($certname) {
      fail("A certificate name is necessary to generate ssl certificate")
    }

    ssl::self_signed_certificate { $certname:
      common_name  => $certname,
      country      => "FR",
      days         => "3650",
      organization => "Immae",
      owner        => $pg_user,
      group        => $pg_group,
      directory    => "$datadir/certs",
    }

    $ssl_key  = "$datadir/certs/$certname.key"
    $ssl_cert = "$datadir/certs/$certname.crt"
  } elsif $copy_keys {
    $ssl_key  = "$datadir/certs/privkey.pem"
    $ssl_cert = "$datadir/certs/cert.pem"

    file { $ssl_cert:
      source  => "file://$cert",
      mode    => "0600",
      links   => "follow",
      owner   => $pg_user,
      group   => $pg_group,
      require => File["$datadir/certs"],
    }
    file { $ssl_key:
      source  => "file://$key",
      mode    => "0600",
      links   => "follow",
      owner   => $pg_user,
      group   => $pg_group,
      require => File["$datadir/certs"],
    }
  } else {
    $ssl_key  = $key
    $ssl_cert = $cert
  }

  if $handle_config_entry {
    postgresql::server::config_entry { "ssl":
      value => "on",
    }

    postgresql::server::config_entry { "ssl_cert_file":
      value => $ssl_cert,
    }

    postgresql::server::config_entry { "ssl_key_file":
      value => $ssl_key,
    }
  } elsif $handle_concat_config {
    concat::fragment { "$datadir/postgresql.conf ssl config":
      target  => "$datadir/postgresql.conf",
      content => "ssl = on\nssl_key_file = '$ssl_key'\nssl_cert_file = '$ssl_cert'\n"
    }
  }
}
