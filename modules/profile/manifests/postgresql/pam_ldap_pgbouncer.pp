class profile::postgresql::pam_ldap_pgbouncer (
  String $pg_user = "postgres"
) {
  include "profile::pam_ldap"

  $password_seed = lookup("base_installation::puppet_pass_seed")
  $ldap_server = lookup("base_installation::ldap_server")
  $ldap_base   = lookup("base_installation::ldap_base")
  $ldap_dn     = lookup("base_installation::ldap_dn")
  $ldap_password = generate_password(24, $password_seed, "ldap")
  $ldap_attribute = "uid"
  $ldap_filter = lookup("role::backup::postgresql::pgbouncer_access_filter", { "default_value" => undef })

  if empty($ldap_filter) {
    fail("need ldap filter for pgbouncer")
  }

  file { "/etc/pam_ldap.d/pgbouncer.conf":
    ensure  => "present",
    mode    => "0600",
    owner   => $pg_user,
    group   => "root",
    content => template("profile/postgresql/pam_ldap_pgbouncer.conf.erb"),
    require => File["/etc/pam_ldap.d"],
  } ->
  file { "/etc/pam.d/pgbouncer":
    ensure => "present",
    mode   => "0644",
    owner  => "root",
    group  => "root",
    source => "puppet:///modules/profile/postgresql/pam_pgbouncer"
  }
}
