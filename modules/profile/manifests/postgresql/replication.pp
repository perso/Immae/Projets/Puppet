define profile::postgresql::replication (
  Boolean          $handle_role   = false,
  Boolean          $handle_config = false,
  Boolean          $add_self_role = false,
  Boolean          $handle_slot   = false,
  Optional[String] $target        = undef,
) {
  include "profile::postgresql::pam_ldap"

  $host_cn = $title
  $host_infos = find_host($facts["ldapvar"]["other"], $host_cn)

  if empty($host_infos) {
    fail("Unable to find host for replication")
  }

  if empty($target) {
    $pg_version = undef
  } else {
    $pg_version = "10"
  }

  $host_infos["ipHostNumber"].each |$ip| {
    $infos = split($ip, "/")
    $ipaddress = $infos[0]
    if (length($infos) == 1 and $ipaddress =~ /:/) {
      $mask = "128"
    } elsif (length($infos) == 1) {
      $mask = "32"
    } else {
      $mask = $infos[1]
    }

    postgresql::server::pg_hba_rule { "allow TCP access for replication to user $host_cn from $ipaddress/$mask":
      type               => 'hostssl',
      database           => 'replication',
      user               => $host_cn,
      address            => "$ipaddress/$mask",
      auth_method        => 'pam',
      order              => "06-01",
      target             => $target,
      postgresql_version => $pg_version,
    }
  }

  if $handle_config {
    ensure_resource("postgresql::server::config_entry", "wal_level", {
      value => "logical",
    })
  }

  if $handle_role {
    postgresql::server::role { $host_cn:
      replication => true,
      require => Service["postgresql"],
    }

    if $add_self_role {
      $ldap_cn = lookup("base_installation::ldap_cn")

      # Needed to be replicated to the backup and be able to recover later
      ensure_resource("postgresql::server::role", $ldap_cn, {
        replication => true,
        require => Service["postgresql"],
      })
    }
  }

  if $handle_slot {
    postgresql_replication_slot { regsubst($host_cn, '-', "_", "G"):
      ensure  => present,
      require => Service["postgresql"],
    }
  }
}
