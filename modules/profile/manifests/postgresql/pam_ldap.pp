class profile::postgresql::pam_ldap (
  String $pg_user = "postgres"
) {
  include "profile::pam_ldap"

  $password_seed = lookup("base_installation::puppet_pass_seed")
  $ldap_server = lookup("base_installation::ldap_server")
  $ldap_base   = lookup("base_installation::ldap_base")
  $ldap_dn     = lookup("base_installation::ldap_dn")
  $ldap_password = generate_password(24, $password_seed, "ldap")
  $ldap_attribute = "cn"

  file { "/etc/pam_ldap.d/postgresql.conf":
    ensure  => "present",
    mode    => "0400",
    owner   => $pg_user,
    group   => "root",
    content => template("profile/postgresql/pam_ldap_postgresql.conf.erb"),
    require => File["/etc/pam_ldap.d"],
  } ->
  file { "/etc/pam.d/postgresql":
    ensure => "present",
    mode   => "0644",
    owner  => "root",
    group  => "root",
    source => "puppet:///modules/profile/postgresql/pam_postgresql"
  }
}
