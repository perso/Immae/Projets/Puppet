define profile::postgresql::base_pg_hba_rules (
  Optional[String] $pg_path  = undef,
  String           $pg_user  = "postgres",
  String           $pg_group = "postgres",
) {
  unless empty($pg_path) {
    concat { "$pg_path/pg_hba.conf":
      owner   => $pg_user,
      group   => $pg_group,
      mode    => '0640',
      warn    => true,
      require => File[$pg_path],
    }

    Postgresql::Server::Pg_hba_rule {
      target             => "$pg_path/pg_hba.conf",
      postgresql_version => "10",
    }
  }

  postgresql::server::pg_hba_rule { "$title - local access as postgres user":
    description => 'Allow local access to postgres user',
    type        => 'local',
    database    => 'all',
    user        => $pg_user,
    auth_method => 'ident',
    order       => "00-01",
  }
  postgresql::server::pg_hba_rule { "$title - localhost access as postgres user":
    description => 'Allow localhost access to postgres user',
    type        => 'host',
    database    => 'all',
    user        => $pg_user,
    address     => "127.0.0.1/32",
    auth_method => 'md5',
    order       => "00-02",
  }
  postgresql::server::pg_hba_rule { "$title - localhost ip6 access as postgres user":
    description => 'Allow localhost access to postgres user',
    type        => 'host',
    database    => 'all',
    user        => $pg_user,
    address     => "::1/128",
    auth_method => 'md5',
    order       => "00-03",
  }
  postgresql::server::pg_hba_rule { "$title - deny access to postgresql user":
    description => 'Deny remote access to postgres user',
    type        => 'host',
    database    => 'all',
    user        => $pg_user,
    address     => "0.0.0.0/0",
    auth_method => 'reject',
    order       => "00-04",
  }
  postgresql::server::pg_hba_rule { "$title - local access":
    description => 'Allow local access with password',
    type        => 'local',
    database    => 'all',
    user        => 'all',
    auth_method => 'md5',
    order       => "10-01",
  }

  postgresql::server::pg_hba_rule { "$title - local access with same name":
    description => 'Allow local access with same name',
    type        => 'local',
    database    => 'all',
    user        => 'all',
    auth_method => 'ident',
    order       => "10-02",
  }

}
