define profile::postgresql::backup_replication (
  String $base_path,
  Hash   $pg_infos,
  String $pg_user  = "postgres",
  String $pg_group = "postgres",
) {
  $host_cn = $title

  $host = find_host($facts["ldapvar"]["other"], $host_cn)
  if empty($host) {
    $pg_backup_host = $host_cn
  } elsif has_key($host["vars"], "host") {
    $pg_backup_host = $host["vars"]["host"][0]
  } else {
    $pg_backup_host = $host["vars"]["real_hostname"][0]
  }

  $pg_path = "$base_path/$pg_backup_host/postgresql"

  # Replication folder
  ensure_resource("file", "$base_path/$pg_backup_host", {
    ensure => directory,
  })

  file { $pg_path:
    ensure  => directory,
    owner   => $pg_user,
    group   => $pg_group,
    mode    => "0700",
    require => File["$base_path/$pg_backup_host"],
  }

  # pg_hba.conf
  profile::postgresql::base_pg_hba_rules { $pg_backup_host:
    pg_path => $pg_path
  }

  # postgresql.conf file and ssl
  concat { "$pg_path/postgresql.conf":
    owner => $pg_user,
    group => $pg_group,
    mode  => '0640',
    warn  => true,
  }

  if !empty($host) and has_key($host["vars"], "postgresql_backup_port") {
    $pg_listen_port = $host["vars"]["postgresql_backup_port"][0]

    profile::postgresql::ssl { $pg_path:
      certname             => $host_cn,
      handle_concat_config => true,
      before               => Service["postgresql_backup@$pg_backup_host"]
    }

    concat::fragment { "$pg_path/postgresql.conf listen":
      target  => "$pg_path/postgresql.conf",
      content => "listen_addresses = '*'\nport = $pg_listen_port\n",
    }

    profile::postgresql::replication { $host_cn:
      target => "$pg_path/pg_hba.conf",
    }
  } else {
    $pg_listen_port = undef

    concat::fragment { "$pg_path/postgresql.conf listen":
      target  => "$pg_path/postgresql.conf",
      content => "listen_addresses = ''\n",
    }
  }

  concat::fragment { "$pg_path/postgresql.conf paths":
    target  => "$pg_path/postgresql.conf",
    content => "unix_socket_directories = '$pg_path'\ndata_directory = '$pg_path'\nwal_level = logical\n",
  }

  $password_seed = lookup("base_installation::puppet_pass_seed")
  $pg_host = $pg_backup_host
  $pg_port = $pg_infos["dbport"]
  $ldap_cn = lookup("base_installation::ldap_cn")
  $ldap_password = generate_password(24, $password_seed, "ldap")
  $pg_slot = regsubst($ldap_cn, '-', "_", "G")

  # recovery.conf file
  $primary_conninfo  = "host=$pg_host port=$pg_port user=$ldap_cn password=$ldap_password sslmode=require"
  $primary_slot_name = $pg_slot
  $standby_mode      = "on"

  file { "$pg_path/recovery.conf":
    owner   => $pg_user,
    group   => $pg_group,
    mode    => '0640',
    content => template('postgresql/recovery.conf.erb'),
  }

  # Initial replication
  exec { "pg_basebackup $pg_path":
    cwd         => $pg_path,
    user        => $pg_user,
    creates     => "$pg_path/PG_VERSION",
    environment => ["PGPASSWORD=$ldap_password"],
    command     => "/usr/bin/pg_basebackup -w -h $pg_host -p $pg_port -U $ldap_cn -D $pg_path -S $pg_slot",
    before      => [
      Concat["$pg_path/pg_hba.conf"],
      File["$pg_path/recovery.conf"],
      Concat["$pg_path/postgresql.conf"],
    ]
  }

  # Service
  ensure_resource("file", "/etc/systemd/system/postgresql_backup@.service", {
    mode    => "0644",
    owner   => "root",
    group   => "root",
    content => template("profile/postgresql/postgresql_backup@.service.erb"),
  })

  service { "postgresql_backup@$pg_backup_host":
    enable  => true,
    ensure  => "running",
    require => [
      File["/etc/systemd/system/postgresql_backup@.service"],
      Concat["$pg_path/pg_hba.conf"],
      File["$pg_path/recovery.conf"],
      Concat["$pg_path/postgresql.conf"],
    ],
    subscribe => [
      Concat["$pg_path/pg_hba.conf"],
      File["$pg_path/recovery.conf"],
      Concat["$pg_path/postgresql.conf"],
    ]
  }

  # Dumps
  profile::postgresql::backup_dump { "$base_path/$pg_backup_host":
    pg_port => $pg_listen_port,
  }

}
