class profile::wireguard (
) {
  $password_seed = lookup("base_installation::puppet_pass_seed")

  ensure_packages(["linux-headers"], { before => Package["wireguard-dkms"] })
  ensure_packages(["wireguard-tools", "wireguard-dkms"])

  $host = $facts["ldapvar"]["self"]
  if has_key($host["vars"], "wireguard_ip") {
    $ips = $host["vars"]["wireguard_ip"]
  } else {
    $ips = []
  }

  $private_key = generate_password(32, $password_seed, "wireguard", "curve25519", true)

  if file("/usr/bin/wg", "/dev/null") != "" {
    $puppet_notifies_path = lookup("base_installation::puppet_notifies_path")
    $public_key = generate("/usr/bin/bash", "-c", "echo $private_key | /usr/bin/wg pubkey")
    concat::fragment { "host_ldap add wireguard":
      target  => "$puppet_notifies_path/host_ldap.info",
      content => "puppetVar: wireguard_public=$public_key",
      order   => "00-80"
    }
  }

  file { "/etc/wireguard/network.conf":
    ensure  => "file",
    mode    => "0600",
    content => template("profile/wireguard/network.conf.erb"),
    require => [Package["wireguard-tools"], Package["wireguard-dkms"]],
    notify  => Service["wg-quick@network"],
  }
  ->
  service { "wg-quick@network":
    ensure => "running",
    enable => true,
  }

}
