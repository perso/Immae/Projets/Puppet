class profile::tools {
  ensure_packages(['vim', 'bash-completion', 'net-tools', 'htop', 'ipython', 'jq'])
}
