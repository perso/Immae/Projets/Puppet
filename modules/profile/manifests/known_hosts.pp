class profile::known_hosts (
  Optional[Array]  $hosts = []
) {
  $hosts.each |$host| {
    sshkey { $host["name"]:
      ensure => "present",
      key    => $host["key"],
      type   => $host["type"],
    }
  }
}
