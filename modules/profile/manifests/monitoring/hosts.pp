class profile::monitoring::hosts inherits profile::monitoring::params {
  $real_hostname = lookup("base_installation::real_hostname")

  Nagios_hostgroup {
    ensure  => "present",
    owner   => "naemon",
    group   => "naemon",
    target  => $objects,
    notify  => Service["naemon"],
    before  => Service["naemon"],
    require => File["/etc/naemon"],
  }

  Nagios_host {
    ensure  => "present",
    owner   => "naemon",
    group   => "naemon",
    target  => $objects,
    notify  => Service["naemon"],
    before  => Service["naemon"],
    require => File["/etc/naemon"],
  }

  nagios_hostgroup { "linux-servers":
    alias   => "Linux Servers",
    members => [$real_hostname],
  }

  $host_linux_server = {
    check_command         => "check-host-alive",
    check_interval        => 5,
    check_period          => "24x7",
    contact_groups        => "admins",
    max_check_attempts    => "10",
    notification_interval => "120",
    notification_options  => "d,u,r",
    retry_interval        => "1",
  }

  nagios_host { $real_hostname:
    address => $real_hostname;
  default: * => $host_linux_server,
  }

}
