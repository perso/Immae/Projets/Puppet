class profile::monitoring::commands inherits profile::monitoring {
  ensure_packages(["monitoring-plugins"])

  file { $plugins:
    ensure => "directory",
    owner  => "root",
    group  => "naemon",
    mode   => "0755",
  }

  [
    "check_command",
    "check_md_raid",
    "check_postgres_replication",
    "check_date",
    "check_last_file_date",
  ].each |$file| {
    file { "$plugins/$file":
      ensure => "present",
      owner  => "root",
      group  => "naemon",
      mode   => "0755",
      source => "puppet:///modules/profile/monitoring/$file",
    }
  }

  Nagios_command {
    ensure => "present",
    owner  => "naemon",
    group  => "naemon",
    target => $objects,
    notify => Service["naemon"],
    before => Service["naemon"],
    require => File["/etc/naemon"],
  }

  nagios_command {
    "check-host-alive":
      command_line => '$USER1$/check_ping -H $HOSTADDRESS$ -w 3000.0,80% -c 5000.0,100% -p 5';
    "check_local_disk":
      command_line => '$USER1$/check_disk -w $ARG1$ -c $ARG2$ -p $ARG3$';
    "check_local_procs":
      command_line => '$USER1$/check_procs -w $ARG1$ -c $ARG2$ -s $ARG3$';
    "check_local_load":
      command_line => '$USER1$/check_load -w $ARG1$ -c $ARG2$';
    "check_local_swap":
      command_line => '$USER1$/check_swap -n ok -w $ARG1$ -c $ARG2$';
    "check_ntp":
      command_line => '$USER1$/check_ntp_time -t 30 -q -H 0.arch.pool.ntp.org';
    "check_md_raid":
      command_line => '$USER2$/check_md_raid',
      require      => File["$plugins/check_md_raid"];
    "check_command_output":
      command_line => '$USER2$/check_command -c "$ARG1$" -o "$ARG2$" $ARG3$',
      require      => File["$plugins/check_command"];
    "check_postgresql_replication":
      command_line => '/usr/bin/sudo -u postgres $USER2$/check_postgres_replication "$ARG1$" "$ARG2$" "$ARG3$"',
      require      => File["$plugins/check_postgres_replication"];
    "check_last_file_date":
      command_line => '$USER2$/check_last_file_date "$ARG1$" "$ARG2$" "$ARG3$"',
      require      => File["$plugins/check_last_file_date"];
    "check_date":
      command_line => '$USER2$/check_date "$ARG1$" "$ARG2$" "$ARG3$"',
      require      => File["$plugins/check_date"];
  }

  unless empty($naemon_url) {
    nagios_command {
      "notify-master":
        command_line => '/etc/naemon/send_nrdp.sh -H "$HOSTADDRESS$" -s "$SERVICEDESC$" -S "$SERVICESTATEID$" -o "$SERVICEOUTPUT$"',
        require      => File["/etc/naemon/send_nrdp.sh"];
    }
  }
}
