class profile::monitoring::services {

  $warn_1_load  = 1.5*$facts["processorcount"]
  $warn_5_load  = $facts["processorcount"]
  $warn_15_load = $facts["processorcount"]
  $warn_load = join([$warn_1_load, $warn_5_load, $warn_15_load], ",")

  $max_1_load  = 2*$facts["processorcount"]
  $max_5_load  = $facts["processorcount"]
  $max_15_load = $facts["processorcount"]
  $max_load = join([$max_1_load, $max_5_load, $max_15_load], ",")

  profile::monitoring::local_service {
    "Size on root partition":
      local => {
        check_command => "check_local_disk!20%!10%!/",
      };
    "Total number of process":
      local => {
        check_command => "check_local_procs!50!100!RSZDT",
      };
    "Average load":
      local => {
        check_command => "check_local_load!$warn_load!$max_load",
      };
    "Swap usage":
      local => {
        check_command => "check_local_swap!20!10",
      };
    "fail2ban is active":
      sudos   => {
        "naemon-fail2ban" => "naemon  ALL=(root) NOPASSWD: /usr/bin/fail2ban-client ping",
      },
      local   => {
        check_command => "check_command_output!fail2ban-client ping!pong!-r root",
      };
    "NTP is activated and working":
      master => {
        check_interval      => "60",
        freshness_threshold => Integer(60*60*1.5),
      },
      local  => {
        check_command  => "check_ntp",
        check_interval => "60"
      };
    "No mdadm array is degraded":
      common => {
        ensure => (find_file("/proc/mdstat") == undef) ? { true => "absent", default =>"present" },
      },
      local => {
        check_command => "check_md_raid",
      };
  }

  Profile::Monitoring::Local_service <| |>
  Profile::Monitoring::External_service <| |>
}
