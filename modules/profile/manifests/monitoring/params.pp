class profile::monitoring::params {
  $real_hostname = lookup("base_installation::real_hostname")

  $services_for_master = "/etc/naemon/services_for_master.cfg"
  $objects             = "/etc/naemon/objects.cfg"
  $plugins             = "/etc/naemon/monitoring-plugins"

  $service_types = {
    "passive" => "external-passive-service",
    "web"     => "external-web-service",
  }

  $service_generic = {
    active_checks_enabled        => "1",
    check_freshness              => "0",
    check_interval               => "10",
    check_period                 => "24x7",
    contact_groups               => "admins",
    event_handler_enabled        => "1",
    flap_detection_enabled       => "1",
    is_volatile                  => "0",
    max_check_attempts           => "3",
    notification_interval        => "60",
    notification_options         => "w,u,c,r",
    notification_period          => "24x7",
    notifications_enabled        => "0",
    obsess_over_service          => "1",
    passive_checks_enabled       => "1",
    process_perf_data            => "1",
    retain_nonstatus_information => "1",
    retain_status_information    => "1",
    retry_interval               => "2",
  }

  $service_local = merge($service_generic, {
    host_name          => $real_hostname,
    check_interval     => "5",
    max_check_attempts => "4",
    retry_interval     => "1",
    })

  $service_local_for_master = {
    host_name           => $service_local["host_name"],
    check_interval      => $service_local["check_interval"],
    retry_interval      => $service_local["retry_interval"],
    freshness_threshold => Integer(60 * Integer($service_local["check_interval"]) * 1.5),
    use                 => $service_types["passive"],
  }
}
