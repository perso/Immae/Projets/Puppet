define profile::monitoring::local_service (
  Optional[Hash] $sudos  = {},
  Optional[Hash] $common = {},
  Optional[Hash] $master = {},
  Optional[Hash] $local  = {},
) {
  include profile::monitoring::params

  $service_description = $title

  Nagios_service {
    ensure  => "present",
    owner   => "naemon",
    group   => "naemon",
    notify  => Service["naemon"],
    before  => Service["naemon"],
    require => File["/etc/naemon"],
  }

  $sudos.each |$sudo_name, $content| {
    ensure_resource("sudo::conf", $sudo_name, {
      content => $content,
      before  => Nagios_service[$service_description],
    })
  }

  $service_group = "${::profile::monitoring::params::service_local[host_name]}-servicegroup"
  ensure_resource("nagios_servicegroup", $service_group, {
    target => $::profile::monitoring::params::services_for_master,
  })

  [true, false].each |$services_for_master| {
    if $services_for_master {
      $default_local = merge(
        $::profile::monitoring::params::service_local_for_master,
        {
          mode           => "0644",
          target         => $::profile::monitoring::params::services_for_master,
          servicegroups  => $service_group,
          notify         => [],
        })
      $t = "master - "
      $services_key = $master
    } else {
      $default_local = merge(
        $::profile::monitoring::params::service_local,
        { target => $::profile::monitoring::params::objects }
      )
      $t = ""
      $services_key = $local
    }

    $hash = merge($default_local, $common, $services_key)

    nagios_service { "$t$service_description":
      service_description => $service_description,
      *                   => $hash
    }
  }
}
