define profile::monitoring::external_service (
  Optional[String] $type   = undef,
  Optional[Hash]   $master = {},
) {
  include profile::monitoring::params
  $service_description = $title

  nagios_service { $service_description:
    service_description => $service_description,
    host_name           => $::profile::monitoring::params::service_local["host_name"],
    use                 => $::profile::monitoring::params::service_types[$type],
    target              => $::profile::monitoring::params::services_for_master,
    *                   => $master,
  }

}
