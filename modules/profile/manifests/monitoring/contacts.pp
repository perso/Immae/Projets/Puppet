class profile::monitoring::contacts inherits profile::monitoring::params {
  Nagios_contactgroup {
    ensure  => "present",
    owner   => "naemon",
    group   => "naemon",
    target  => $objects,
    notify  => Service["naemon"],
    before  => Service["naemon"],
    require => File["/etc/naemon"],
  }

  nagios_contactgroup { "admins":
    alias => "System administrators",    
  }

}
