class profile::xmr_stak (
  String           $mining_pool,
  String           $wallet,
  Optional[String] $cpulimit = "50",
  Optional[String] $password = "x",
) {
  unless empty($mining_pool) or $cpulimit == "0" {
    ensure_resource('exec', 'systemctl daemon-reload', {
      command     => '/usr/bin/systemctl daemon-reload',
      refreshonly =>  true
    })

    ensure_packages(["cpulimit"])
    aur::package { "xmr-stak_cpu":
      ensure => "absent"
    } ->
    aur::package { "xmr-stak": }
    ~>
    exec { "remove cpu.txt":
      command     => "/usr/bin/rm /var/lib/xmr_stak/cpu.txt",
      refreshonly => true,
      before      => Service["xmr-stak"],
    }

    user { "xmr_stak":
      name       => "xmr_stak",
      ensure     => "present",
      managehome => true,
      home       => "/var/lib/xmr_stak",
      system     => true,
      password   => "!!",
      require    => Aur::Package["xmr-stak"],
    }

    file { "/etc/systemd/system/xmr-stak.service":
      mode    => "0644",
      owner   => "root",
      group   => "root",
      content => template("profile/xmr_stak/xmr-stak.service.erb"),
      require => User["xmr_stak"],
      notify  => Exec["systemctl daemon-reload"]
    }

    $instance = regsubst(lookup("base_installation::ldap_cn"), '\.', "_", "G")

    file { "/var/lib/xmr_stak/xmr-stak.conf":
      mode    => "0644",
      owner   => "root",
      group   => "root",
      content => template("profile/xmr_stak/xmr-stak.conf.erb"),
      require => User["xmr_stak"],
    }

    service { "xmr-stak":
      enable    => true,
      ensure    => "running",
      subscribe => [
        Aur::Package["xmr-stak"],
        File["/var/lib/xmr_stak/xmr-stak.conf"],
        File["/etc/systemd/system/xmr-stak.service"]
      ],
      require   => [
        Aur::Package["xmr-stak"],
        File["/var/lib/xmr_stak/xmr-stak.conf"],
        File["/etc/systemd/system/xmr-stak.service"]
      ]
    }
  }
}

