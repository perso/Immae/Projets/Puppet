class profile::kerberos::client {
  ensure_packages(["krb5", "cyrus-sasl-gssapi"])

  file { "/etc/krb5.conf":
    source => "puppet:///modules/profile/kerberos/krb5_client.conf"
  }
}
