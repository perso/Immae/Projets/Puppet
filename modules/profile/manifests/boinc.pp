class profile::boinc (
  Optional[String] $rpc_password = undef,
  Optional[String] $rpc_host     = undef,
) {
  unless empty($rpc_host) or empty($rpc_password) {
    ensure_packages(["boinc-nox", "lib32-glibc", "lib32-glib2"], { ensure => latest })

    file { "/var/lib/boinc/gui_rpc_auth.cfg":
      ensure  => present,
      content => $rpc_password,
      owner   => "boinc",
      group   => "boinc",
      mode    => "0600",
      require => Package["boinc-nox"]
    }
    ->
    file { "/var/lib/boinc/remote_hosts.cfg":
      ensure  => present,
      content => $rpc_host,
      owner   => "boinc",
      group   => "boinc",
      mode    => "0600",
    }
    ->
    service { "boinc":
      enable => false,
      ensure => "stopped",
    } ->
    service { "boinc-client":
      enable => true,
      ensure => "running",
    }
  }
}
