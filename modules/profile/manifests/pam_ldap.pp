class profile::pam_ldap (
) {
  ensure_packages(["pam_ldap"])

  file { "/etc/pam_ldap.d":
    ensure  => directory,
    mode    => "0755",
    owner   => "root",
    group   => "root",
    require => Package["pam_ldap"],
  }
}

