class profile::redis {
  file { '/etc/systemd/system/redis.service.d/':
    ensure => "directory",
    path   => "/etc/systemd/system/redis.service.d/",
    mode   => "0755",
    owner  => "root",
    group  => "root"
  } ->
  file { '/etc/systemd/system/redis.service.d/socket_shutdown.conf':
    ensure  => "present",
    path    => "/etc/systemd/system/redis.service.d/noclear.conf",
    source  => 'puppet:///modules/profile/redis/socket_shutdown_override.conf',
    recurse => true,
    mode    => "0644",
    owner   => "root",
    group   => "root",
    notify  => Service["redis"],
  }

  ensure_packages(["ruby-augeas"])

  class { '::redis':
    unixsocket       => "/run/redis/redis.sock",
    unixsocketperm   => "777",
    ulimit           => false,
    daemonize        => false,
    config_file      => "/etc/redis.conf",
    config_file_orig => "/etc/redis.conf.puppet",
    port             => 0,
    require          => [
      File["/etc/systemd/system/redis.service.d/socket_shutdown.conf"],
      Package["ruby-augeas"]
    ]
  }

}
