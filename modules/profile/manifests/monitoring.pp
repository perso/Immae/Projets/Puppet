class profile::monitoring (
  Optional[String] $naemon_url   = undef,
  Optional[String] $naemon_token = undef,
) inherits profile::monitoring::params {
  $real_hostname = lookup("base_installation::real_hostname")

  ensure_packages(["naemon", "cnagios"], { ensure => latest })

  file { "/etc/naemon":
    ensure  => "directory",
    recurse => true,
    purge   => true,
    force   => true,
    require => Package["naemon"],
  }
  ->
  file { "/etc/naemon/resource.cfg":
    ensure  => "file",
    owner   => "naemon",
    group   => "naemon",
    mode    => "0600",
    content => template("profile/monitoring/resource.cfg.erb"),
  }
  ->
  file { "/etc/naemon/naemon.cfg":
    ensure  => "file",
    owner   => "naemon",
    group   => "naemon",
    mode    => "0644",
    content => template("profile/monitoring/naemon.cfg.erb"),
  }
  ->
  file { $objects:
    ensure => "file",
    owner  => "naemon",
    group  => "naemon",
    mode   => "0600"
  }
  ->
  service { "naemon":
    ensure => "running",
    enable => true,
  }

  file { "/usr/local/sbin/i_naemon_force_check":
    ensure  => present,
    mode    => "0755",
    content => template("profile/monitoring/force_check.sh.erb"),
  }

  unless ($naemon_url == undef or empty($naemon_url)) {
    file { "/etc/naemon/send_nrdp.sh":
      ensure  => "file",
      owner   => "naemon",
      group   => "naemon",
      mode    => "0700",
      content => template("profile/monitoring/send_nrdp.sh.erb"),
    }
  }

  include "profile::monitoring::hosts"
  include "profile::monitoring::services"
  include "profile::monitoring::commands"
  include "profile::monitoring::times"
  include "profile::monitoring::contacts"
}
