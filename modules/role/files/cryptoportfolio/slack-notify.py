#!/usr/bin/env python3

import os
import sys
import json
import urllib3

project = os.environ["P_PROJECT"]
url     = os.environ["P_WEBHOOK"]
version = os.environ["P_VERSION"]
host    = os.environ["P_HOST"]
if os.environ["P_HTTPS"] == "true":
    scheme = "https://"
else:
    scheme = "http://"

def post(url, data):
    urllib3.disable_warnings()
    http = urllib3.PoolManager()

    encoded = data.encode('utf-8')

    r = http.request("POST", url,
            headers={'Content-Type': 'application/json'},
            body=encoded)

data = {
        "username": "Puppet",
        "icon_url": "https://learn.puppet.com/static/images/logos/Puppet-Logo-Mark-Amber.png",
        "text": "Deployed {} of {} on {}{}".format(
            version,
            project,
            scheme,
            host,
            ),
        }

json_data = json.dumps(data)
post(url, json_data)
