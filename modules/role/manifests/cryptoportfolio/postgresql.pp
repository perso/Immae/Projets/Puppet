class role::cryptoportfolio::postgresql inherits role::cryptoportfolio {
  $password_seed = lookup("base_installation::puppet_pass_seed")

  $pg_password = generate_password(24, $password_seed, "postgres_cryptoportfolio")

  profile::postgresql::master { "postgresql master for cryptoportfolio":
    letsencrypt_host => $web_host,
    backup_hosts     => ["backup-1"],
  }

  postgresql::server::db { $pg_db:
    user     =>  $pg_user,
    password =>  postgresql_password($pg_user, $pg_password),
  }

  postgresql::server::pg_hba_rule { 'allow local access to cryptoportfolio user':
    type        => 'local',
    database    => $pg_db,
    user        => $pg_user,
    auth_method => 'ident',
    order       => "05-01",
  }

}
