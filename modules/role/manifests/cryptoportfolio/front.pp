class role::cryptoportfolio::front inherits role::cryptoportfolio {
  ensure_resource('exec', 'systemctl daemon-reload', {
    command     => '/usr/bin/systemctl daemon-reload',
    refreshonly =>  true
  })

  $password_seed = lookup("base_installation::puppet_pass_seed")

  $cf_front_app = "${home}/go/src/git.immae.eu/Cryptoportfolio/Front"
  $cf_front_app_api_workdir = "${cf_front_app}/cmd/app"
  $cf_front_app_api_bin = "${cf_front_app_api_workdir}/cryptoportfolio-app"
  $cf_front_app_api_conf = "${home}/conf.toml"
  $cf_front_app_api_secret = generate_password(24, $password_seed, "cryptoportfolio_api_secret")

  $cf_front_app_password_reset_secret = generate_password(24, $password_seed, "cryptoportfolio_api_password_reset_secret")
  $cf_front_app_free_sms_user = lookup("role::cryptoportfolio::front::free_sms_user")
  $cf_front_app_free_sms_pass = lookup("role::cryptoportfolio::front::free_sms_pass")

  $cf_front_app_static_conf = "${cf_front_app}/cmd/web/env/prod.env"

  ensure_packages(["go", "npm", "nodejs", "yarn"])

  file { [
      "${home}/go/",
      "${home}/go/src",
      "${home}/go/src/git.immae.eu",
      "${home}/go/src/git.immae.eu/Cryptoportfolio",
      $cf_front_app]:
    ensure  => "directory",
    mode    => "0700",
    owner   => $user,
    group   => $group,
    require => User["$user:"],
  }

  file { "${home}/front":
    ensure  => "link",
    target  => $cf_front_app,
    before => File[$cf_front_app],
  }

  file { "/etc/systemd/system/cryptoportfolio-app.service":
    mode    => "0644",
    owner   => "root",
    group   => "root",
    content => template("role/cryptoportfolio/cryptoportfolio-app.service.erb"),
    notify  => Exec["systemctl daemon-reload"],
  }

  service { 'cryptoportfolio-app':
    enable    => true,
    ensure    => "running",
    subscribe => [File[$cf_front_app_api_conf], Exec["go-cryptoportfolio-app"], Exec["web-cryptoportfolio-build"]],
    require   => [
      File["/etc/systemd/system/cryptoportfolio-app.service"],
      Postgresql::Server::Db[$pg_db]
    ],
  } ~>
  exec { "dump $pg_db structure":
    refreshonly => true,
    user        => $::profile::postgresql::pg_user,
    group       => $::profile::postgresql::pg_user,
    command     => "/usr/bin/pg_dump --schema-only --clean --no-publications $pg_db > /var/lib/postgres/${pg_db}.schema",
  }

  archive { "${home}/front_${front_version}.tar.gz":
    path          => "${home}/front_${front_version}.tar.gz",
    source        => "https://release.immae.eu/cryptoportfolio/front/front_${front_version}.tar.gz",
    checksum_type => "sha256",
    checksum      => $front_sha256,
    cleanup       => false,
    extract       => true,
    user          => $user,
    username      => lookup("base_installation::ldap_cn"),
    password      => generate_password(24, $password_seed, "ldap"),
    extract_path  => $cf_front_app,
    require       => [User["$user:"], File[$cf_front_app]],
    notify        => [
      Exec["web-cryptoportfolio-dependencies"],
      Exec["go-get-dep"],
    ]
  }

  # Api
  $pg_password = generate_password(24, $password_seed, "postgres_cryptoportfolio")
  $pg_host = "${pg_hostname}:${pg_port}"
  file { $cf_front_app_api_conf:
    owner   => $user,
    group   => $group,
    mode    => "0600",
    content => template("role/cryptoportfolio/api_conf.toml.erb"),
    before  => Exec["go-cryptoportfolio-app"],
  }

  exec { "go-get-dep":
    user        => $user,
    environment => ["HOME=${home}"],
    creates     => "${home}/go/bin/dep",
    command     => "/usr/bin/go get -u github.com/golang/dep/cmd/dep",
    refreshonly => true,
  } ~>
  exec { "go-cryptoportfolio-dependencies":
    cwd         => $cf_front_app,
    user        => $user,
    environment => ["HOME=${home}"],
    command     => "${home}/go/bin/dep ensure",
    refreshonly => true,
  } ~>
  exec { "go-cryptoportfolio-app":
    cwd         => $cf_front_app_api_workdir,
    user        => $user,
    environment => ["HOME=${home}"],
    command     => "/usr/bin/make build",
    refreshonly => true,
  }

  # Static pages
  file { $cf_front_app_static_conf:
    owner   => $user,
    group   => $group,
    mode    => "0600",
    content => template("role/cryptoportfolio/static_conf.env.erb"),
    before  => Exec["web-cryptoportfolio-build"],
  }

  exec { "web-cryptoportfolio-dependencies":
    cwd         => "${cf_front_app}/cmd/web",
    user        => $user,
    environment => ["HOME=${home}"],
    command     => "/usr/bin/make install",
    refreshonly => true,
    require     => [Package["npm"], Package["nodejs"], Package["yarn"]]
  } ~>
  exec { "web-cryptoportfolio-build":
    cwd         => "${cf_front_app}/cmd/web",
    user        => $user,
    environment => ["HOME=${home}"],
    path        => ["${cf_front_app}/cmd/web/node_modules/.bin/", "/usr/bin"],
    command     => "/usr/bin/make static ENV=${env}",
    refreshonly => true,
  }

  # Slack logger
  $slack_logger = lookup("role::cryptoportfolio::front::slack_logger")
  $slack_logger_username = lookup("role::cryptoportfolio::front::slack_logger_username", { "default_value" =>  "Logger" })

  unless empty($slack_logger) {
    file { "/usr/local/bin/api_logger":
      mode    => "0755",
      content => template("role/cryptoportfolio/api_logger.py.erb"),
      notify  => Service["cryptoportfolio-log"],
    }
    ->
    file { "/etc/systemd/system/cryptoportfolio-log.service":
      mode    => "0644",
      owner   => "root",
      group   => "root",
      content => template("role/cryptoportfolio/cryptoportfolio-log.service.erb"),
      notify  => [
        Exec["systemctl daemon-reload"],
        Service["cryptoportfolio-log"]
      ]
    }
    ->
    service { 'cryptoportfolio-log':
      enable    => true,
      ensure    => "running",
      require   => [
        Service["cryptoportfolio-app"],
      ],
    }
  }

  unless empty($webhook_url) {
    exec { "front-slack-notify":
      refreshonly => true,
      environment => [
        "P_PROJECT=Front",
        "P_WEBHOOK=${webhook_url}",
        "P_VERSION=${front_version}",
        "P_HOST=${web_host}",
        "P_HTTPS=${web_ssl}",
      ],
      command     => "/usr/local/bin/slack-notify",
      require     => File["/usr/local/bin/slack-notify"],
      subscribe   => [Exec["go-cryptoportfolio-app"], Exec["web-cryptoportfolio-build"]],
    }
  }

  @profile::monitoring::external_service { "Cryptoportfolio website is running on $web_host":
    type   => "web",
    master => {
      check_command => "check_https!$web_host!/!<title>Cryptoportfolio"
    }
  }
  @profile::monitoring::external_service { "$web_host ssl certificate is up to date":
    type   => "web",
    master => {
      check_command => "check_https_certificate!$web_host"
    }
  }
}
