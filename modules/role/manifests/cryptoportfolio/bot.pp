class role::cryptoportfolio::bot inherits role::cryptoportfolio {
  $password_seed = lookup("base_installation::puppet_pass_seed")

  $cf_bot_app = "${home}/bot"
  $cf_bot_app_conf = "${home}/bot_config.ini"
  $cf_bot_app_conf_hourly = "${home}/bot_config_hourly.ini"
  $cf_bot_app_reports = "${home}/bot_reports"

  ensure_packages(["python", "python-pip"])

  file { $cf_bot_app:
    ensure  => "directory",
    mode    => "0700",
    owner   => $user,
    group   => $group,
    require => User["$user:"],
  }

  archive { "${home}/trader_${trader_version}.tar.gz":
    path          => "${home}/trader_${trader_version}.tar.gz",
    source        => "https://release.immae.eu/cryptoportfolio/trader/trader_${trader_version}.tar.gz",
    checksum_type => "sha256",
    checksum      => $trader_sha256,
    cleanup       => false,
    extract       => true,
    user          => $user,
    username      => lookup("base_installation::ldap_cn"),
    password      => generate_password(24, $password_seed, "ldap"),
    extract_path  => $cf_bot_app,
    require       => [User["$user:"], File[$cf_bot_app]],
  } ~>
  exec { "py-cryptoportfolio-dependencies":
    cwd         => $cf_bot_app,
    user        => $user,
    environment => ["HOME=${home}"],
    command     => "/usr/bin/make install",
    require     => User["$user:"],
    refreshonly => true,
    before      => [
      File[$cf_bot_app_conf],
      Cron::Job::Multiple["py-cryptoportfolio"],
    ]
  }

  $pg_password = generate_password(24, $password_seed, "postgres_cryptoportfolio")
  file { $cf_bot_app_conf:
    owner   => $user,
    group   => $group,
    mode    => "0600",
    content => template("role/cryptoportfolio/bot_config.ini.erb"),
    require => [
      User["$user:"],
      Archive["${home}/trader_${trader_version}.tar.gz"],
    ],
  }

  cron::job::multiple { "py-cryptoportfolio":
    ensure      => present,
    environment => ["HOME=${home}","PATH=/usr/bin/","CRON_TZ=UTC"],
    require     => [
      File[$cf_bot_app_conf],
      Archive["${home}/trader_${trader_version}.tar.gz"]
    ],
    jobs        => [],
  }

  if ($environment == "production") {
    $monitored_key = 3
    @profile::monitoring::local_service { "Last redis report is less than 2 hours old":
      local => {
        check_command => "check_date!redis-cli -s $redis_host GET /cryptoportfolio/$monitored_key/latest/date!2"
      }
    }

    @profile::monitoring::local_service { "Last bot report is less than one week old":
      sudos => {
        "naemon-cryptoportfolio-bot" => "naemon  ALL=($user) NOPASSWD: /usr/bin/find $cf_bot_app_reports -mindepth 1 -maxdepth 1 -printf %T@?n",
      },
      local => {
        check_command => "check_last_file_date!$cf_bot_app_reports!168!$user",
      },
    }
  }

  if versioncmp($trader_version, "v1.3") >= 0 {
    file { $cf_bot_app_conf_hourly:
      owner   => $user,
      group   => $group,
      mode    => "0600",
      content => template("role/cryptoportfolio/bot_config_hourly.ini.erb"),
      require => [
        User["$user:"],
        Archive["${home}/trader_${trader_version}.tar.gz"],
      ],
    }

    cron::job::multiple { "py-cryptoportfolio-hourly":
      ensure      => present,
      environment => ["HOME=${home}","PATH=/usr/bin/"],
      require     => [
        File[$cf_bot_app_conf_hourly],
        Archive["${home}/trader_${trader_version}.tar.gz"]
      ],
      jobs        => [
        {
          command     => "cd $cf_bot_app ; python main.py --config $cf_bot_app_conf_hourly",
          user        => $user,
          minute      => 30,
          description => "Print the current state to redis",
        },
      ],
    }
  }

  unless empty($webhook_url) {
    exec { "bot-slack-notify":
      refreshonly => true,
      environment => [
        "P_PROJECT=Trader",
        "P_WEBHOOK=${webhook_url}",
        "P_VERSION=${trader_version}",
        "P_HOST=${web_host}",
        "P_HTTPS=${web_ssl}",
      ],
      command     => "/usr/local/bin/slack-notify",
      require     => File["/usr/local/bin/slack-notify"],
      subscribe   => Exec["py-cryptoportfolio-dependencies"],
    }
  }
}

