class role::cryptoportfolio::apache inherits role::cryptoportfolio {
  class { 'apache::mod::headers': }
  apache::vhost { $web_host:
    port                => '443',
    docroot             => false,
    manage_docroot      => false,
    proxy_dest          => "http://localhost:8000",
    request_headers     => 'set X-Forwarded-Proto "https"',
    ssl                 => true,
    ssl_cert            => "/etc/letsencrypt/live/$web_host/cert.pem",
    ssl_key             => "/etc/letsencrypt/live/$web_host/privkey.pem",
    ssl_chain           => "/etc/letsencrypt/live/$web_host/chain.pem",
    require             => Letsencrypt::Certonly[$web_host],
    proxy_preserve_host => true;
    default: *          => $::profile::apache::apache_vhost_default;
  }
}
