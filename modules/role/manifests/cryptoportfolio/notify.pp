class role::cryptoportfolio::notify inherits role::cryptoportfolio {
  file { "/usr/local/bin/slack-notify":
    mode   => "0755",
    source => "puppet:///modules/role/cryptoportfolio/slack-notify.py",
  }
}
