class role::file_store (
  Optional[Hash]  $nfs_mounts = {},
  Optional[String] $mountpoint = "/fichiers1",
) {
  include "base_installation"

  include "profile::fstab"
  include "profile::tools"
  include "profile::monitoring"
  include "profile::wireguard"

  unless empty($mountpoint) {
    class { "::nfs":
      server_enabled             => true,
      nfs_v4                     => true,
      nfs_v4_export_root         => '/exports',
      nfs_v4_export_root_clients => 'localhost(rw)',
      require                    => Mount[$mountpoint],
    }

    $nfs_mounts.each |$nfs_mount, $hosts| {
      file { "$mountpoint/$nfs_mount":
        ensure  => "directory",
        mode    => "0755",
        owner   => "nobody",
        group   => "nobody",
        require => Mount[$mountpoint],
      }

      $hosts.each |$host_cn| {
        $host = find_host($facts["ldapvar"]["other"], $host_cn)
        if empty($host) {
          fail("No host found for nfs")
        } elsif has_key($host["vars"], "wireguard_ip") {
          $clients = sprintf("%s%s",
            join($host["vars"]["wireguard_ip"], "(rw,secure,sync,all_squash) "),
            "(rw,secure,sync,all_squash)")
          nfs::server::export { "$mountpoint/$nfs_mount":
            owner   => "nobody",
            group   => "nobody",
            ensure  => "present",
            clients => $clients,
          }
        } elsif has_key($host["vars"], "host") {
          nfs::server::export { "$mountpoint/$nfs_mount":
            owner   => "nobody",
            group   => "nobody",
            ensure  => "present",
            clients => "${host[vars][host][0]}(rw,secure,sync,all_squash)",
          }
        } else {
          nfs::server::export { "$mountpoint/$nfs_mount":
            owner   => "nobody",
            group   => "nobody",
            ensure  => "present",
            clients => "${host[vars][real_hostname][0]}(rw,secure,sync,all_squash)",
          }
        }
      }
    }
  }
}
