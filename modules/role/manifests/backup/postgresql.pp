class role::backup::postgresql inherits role::backup {
  ensure_packages(["postgresql"])

  $pg_backup_hosts = lookup("role::backup::postgresql::backup_hosts", { "default_value" => {} })

  $pg_backup_hosts.each |$backup_host_cn, $pg_infos| {
    profile::postgresql::backup_replication { $backup_host_cn:
      base_path => $mountpoint,
      pg_infos  => $pg_infos,
    }

    if $pg_infos["pgbouncer"] {
      profile::postgresql::backup_pgbouncer { $backup_host_cn:
        base_path => $mountpoint,
        pg_infos  => $pg_infos,
      }
    }

  }

}
