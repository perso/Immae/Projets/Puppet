class role::etherpad (
  String $web_host,
) {
  $password_seed = lookup("base_installation::puppet_pass_seed")
  $real_host   = lookup("base_installation::real_hostname")
  $web_listen  = "127.0.0.1"
  $web_port    = 18000
  $pg_db       = "etherpad-lite"
  $pg_user     = "etherpad-lite"
  $pg_password = generate_password(24, $password_seed, "postgres_etherpad")

  $ldap_server = lookup("base_installation::ldap_server")
  $ldap_base   = lookup("base_installation::ldap_base")
  $ldap_dn     = lookup("base_installation::ldap_dn")
  $ldap_account_pattern = "(&(memberOf=cn=users,cn=etherpad,ou=services,dc=immae,dc=eu)(uid={{username}}))"
  $ldap_group_pattern = "(memberOf=cn=groups,cn=etherpad,ou=services,dc=immae,dc=eu)"
  $ldap_password = generate_password(24, $password_seed, "ldap")


  include "base_installation"

  include "profile::tools"
  include "profile::postgresql"
  include "profile::apache"
  include "profile::monitoring"

  ensure_packages(["npm"])
  ensure_packages(["abiword"])
  ensure_packages(["libreoffice-fresh", "libreoffice-fresh-fr", "java-runtime-common", "jre8-openjdk"])
  ensure_packages(["tidy"])
  aur::package { "etherpad-lite": }
  -> patch::file { "/usr/share/etherpad-lite/src/node/utils/LibreOffice.js":
    diff_source => "puppet:///modules/role/etherpad/libreoffice_patch.diff",
  }
  -> file { "/etc/etherpad-lite/settings.json":
    ensure  => present,
    owner   => "etherpad-lite",
    group   => "etherpad-lite",
    notify  => Service["etherpad-lite"],
    content => template("role/etherpad/settings.json.erb"),
  }

  $modules = [
    "ep_aa_file_menu_toolbar",
    "ep_adminpads",
    "ep_align",
    "ep_bookmark",
    "ep_clear_formatting",
    "ep_colors",
    "ep_copy_paste_select_all",
    "ep_cursortrace",
    "ep_embedmedia",
    "ep_font_family",
    "ep_font_size",
    "ep_headings2",
    "ep_ldapauth",
    "ep_line_height",
    "ep_markdown",
    "ep_previewimages",
    "ep_ruler",
    "ep_scrollto",
    "ep_set_title_on_pad",
    "ep_subscript_and_superscript",
    "ep_timesliderdiff"
    ]

  $modules.each |$module| {
    exec { "npm_install_$module":
      command     => "/usr/bin/npm install $module",
      unless      => "/usr/bin/test -d /usr/share/etherpad-lite/node_modules/$module",
      cwd         => "/usr/share/etherpad-lite/",
      environment => "HOME=/root",
      require     => Aur::Package["etherpad-lite"],
      before      => Service["etherpad-lite"],
      notify      => Service["etherpad-lite"],
    }
    ->
    file { "/usr/share/etherpad-lite/node_modules/$module/.ep_initialized":
      ensure => present,
      mode   => "0644",
      before => Service["etherpad-lite"],
    }
  }

  service { "etherpad-lite":
    enable    => true,
    ensure    => "running",
    require   => [Aur::Package["etherpad-lite"], Service["postgresql"]],
    subscribe => Aur::Package["etherpad-lite"],
  }

  profile::postgresql::master { "postgresql master for etherpad":
    letsencrypt_host => $real_host,
    backup_hosts     => ["backup-1"],
  }

  postgresql::server::db { $pg_db:
    user     =>  $pg_user,
    password =>  postgresql_password($pg_user, $pg_password),
  }

  postgresql::server::pg_hba_rule { "allow local access to $pg_user user":
    type        => 'local',
    database    => $pg_db,
    user        => $pg_user,
    auth_method => 'ident',
    order       => "05-01",
  }

  class { 'apache::mod::headers': }
  apache::vhost { $web_host:
    port                => '443',
    docroot             => false,
    manage_docroot      => false,
    proxy_dest          => "http://localhost:18000",
    request_headers     => 'set X-Forwarded-Proto "https"',
    ssl                 => true,
    ssl_cert            => "/etc/letsencrypt/live/$web_host/cert.pem",
    ssl_key             => "/etc/letsencrypt/live/$web_host/privkey.pem",
    ssl_chain           => "/etc/letsencrypt/live/$web_host/chain.pem",
    require             => Letsencrypt::Certonly[$web_host],
    proxy_preserve_host => true;
    default: *          => $::profile::apache::apache_vhost_default;
  }

  @profile::monitoring::external_service { "Etherpad service is running on $web_host":
    type   => "web",
    master => {
      check_command => "check_https!$web_host!/!<title>Etherpad"
    }
  }
  @profile::monitoring::external_service { "$web_host ssl certificate is up to date":
    type   => "web",
    master => {
      check_command => "check_https_certificate!$web_host"
    }
  }
}
