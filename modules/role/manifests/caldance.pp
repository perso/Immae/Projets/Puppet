class role::caldance (
  String           $user,
  String           $group,
  String           $home,
  String           $web_host,
  String           $pg_user,
  String           $pg_db,
  String           $mail_from,
  String           $smtp_host,
  String           $smtp_port,
  Optional[String] $pg_hostname      = "/run/postgresql",
  Optional[String] $pg_port          = "5432",
  Optional[String] $caldance_version = undef,
  Optional[String] $caldance_sha256  = undef,
  Optional[Array]  $cron_pip         = [],
) {
  $password_seed = lookup("base_installation::puppet_pass_seed")
  include "base_installation"

  include "profile::mail"
  include "profile::tools"
  include "profile::postgresql"
  include "profile::apache"
  include "profile::redis"
  include "profile::monitoring"

  ensure_packages(["python-pip", "python-virtualenv", "python-django", "uwsgi-plugin-python"])

  $caldance_app = "${home}/app"
  $caldance_app_old = "${home}/app_old"
  $caldance_app_tmp = "${home}/app_tmp"
  $pg_password = generate_password(24, $password_seed, "postgres_caldance")
  $secret_key = generate_password(24, $password_seed, "secret_key_caldance")
  $socket = "/run/caldance/app.sock"

  $uwsgi_path = "${home}/virtualenv/bin/uwsgi"
  $python_path = "${home}/virtualenv/bin/python"

  $environment = {
    "DB_NAME"     => $pg_db,
    "DB_USER"     => $pg_user,
    "DB_PASSWORD" => $pg_password,
    "DB_HOST"     => $pg_hostname,
    "DB_PORT"     => $pg_port,
    "SECRET_KEY"  => $secret_key,
    "DEBUG"       => "False",
    "LOG_FILE"    => "$home/caldance.log",
    "MEDIA_ROOT"  => "$home/media",
  }

  file { $home:
    mode => "0755",
  }

  file { $caldance_app_tmp:
    ensure  => "directory",
    mode    => "0755",
    owner   => $user,
    group   => $group,
    require => User["$user:"],
  }
  file { $caldance_app:
    ensure  => "directory",
    mode    => "0755",
    owner   => $user,
    group   => $group,
    require => User["$user:"],
  } ->
  file { "${home}/media":
    ensure => "directory",
    mode   => "0755",
    owner  => $user,
    group  => $group,
  }

  exec { "initialize_venv":
    user    => $user,
    require => User["$user:"],
    command => "/usr/bin/virtualenv ${home}/virtualenv",
    creates => "${home}/virtualenv",
  }
  ->
  archive { "${home}/caldance_${caldance_version}.tar.gz":
    path          => "${home}/caldance_${caldance_version}.tar.gz",
    source        => "https://release.immae.eu/caldance/caldance_${caldance_version}.tar.gz",
    checksum_type => "sha256",
    checksum      => $caldance_sha256,
    cleanup       => false,
    extract       => true,
    user          => $user,
    username      => lookup("base_installation::ldap_cn"),
    password      => generate_password(24, $password_seed, "ldap"),
    extract_path  => $caldance_app_tmp,
    require       => [User["$user:"], File[$caldance_app_tmp]],
  } ~>
  exec { "py-requirements":
    cwd         => $caldance_app_tmp,
    user        => $user,
    environment => ["HOME=${home}"],
    command     => "/usr/bin/sed -i -e '/GDAL/d' requirements.txt && ${home}/virtualenv/bin/pip install -r requirements.txt --upgrade",
    require     => User["$user:"],
    refreshonly => true,
  } ~>
  exec { "stop uwsgi application":
    command     => "/usr/bin/systemctl stop caldance-app.service || /usr/bin/true",
    require     => [User["$user:"]],
    refreshonly => true,
  } ~>
  exec { "mv app_tmp":
    cwd         => $home,
    user        => $user,
    environment => ["HOME=${home}"],
    command     => "/usr/bin/rm -rf $caldance_app_old && /usr/bin/mv $caldance_app $caldance_app_old && /usr/bin/mv $caldance_app_tmp $caldance_app && /usr/bin/mkdir $caldance_app_tmp",
    require     => [User["$user:"]],
    refreshonly => true,
  } ~>
  file { "$caldance_app/manage.py":
    owner   => $user,
    group   => $group,
    mode    => "0755",
    content => template("role/caldance/manage.py.erb"),
    require => [
      User["$user:"],
      Archive[ "${home}/caldance_${caldance_version}.tar.gz"],
    ],
  } ~>
  file { "$caldance_app/manage":
    owner   => $user,
    group   => $group,
    mode    => "0755",
    content => template("role/caldance/manage.sh.erb"),
    require => [
      User["$user:"],
      File["$caldance_app/manage.py"],
      Archive[ "${home}/caldance_${caldance_version}.tar.gz"],
    ],
  } ~>
  file { "$caldance_app/app.ini":
    owner   => $user,
    group   => $group,
    mode    => "0644",
    content => template("role/caldance/app.ini.erb"),
    require => [
      User["$user:"],
      Archive[ "${home}/caldance_${caldance_version}.tar.gz"],
    ],
  } ~>
  exec { "py-migrate":
    cwd         => $caldance_app,
    user        => $user,
    environment => ["HOME=${home}"],
    command     => "$caldance_app/manage migrate",
    require     => [User["$user:"], File["$caldance_app/manage"]],
    refreshonly => true,
  } ~>
  exec { "py-static":
    cwd         => $caldance_app,
    user        => $user,
    environment => ["HOME=${home}"],
    command     => "$caldance_app/manage collectstatic --no-input",
    require     => [User["$user:"], File["$caldance_app/manage"]],
    refreshonly => true,
  } ~>
  exec { "restart uwsgi application":
    command     => "/usr/bin/systemctl restart caldance-app.service",
    require     => [User["$user:"], File["$caldance_app/app.ini"]],
    refreshonly => true,
  }

  profile::postgresql::master { "postgresql master for caldance":
    letsencrypt_host => $web_host,
    backup_hosts     => [],
  }

  postgresql::server::db { $pg_db:
    user     =>  $pg_user,
    password =>  postgresql_password($pg_user, $pg_password),
  }

  # pour le script de génération de mdp
  ensure_packages(["perl-digest-sha1"])

  ensure_packages(["postgis", "python-gdal", "ripgrep"])
  file { "/usr/local/bin/ldap_ssha":
    owner   => "root",
    group   => "root",
    mode    => "0755",
    source  => "puppet:///modules/base_installation/scripts/ldap_ssha",
    require => Package["perl-digest-sha1"],
  }

  sudo::conf { 'wheel_nopasswd':
    priority => 99,
    content  => "%wheel ALL=(ALL) NOPASSWD: ALL",
    require  => Package["sudo"],
  }

  ensure_packages(["mod_wsgi"])
  class { 'apache::mod::wsgi':
    wsgi_python_home => "${home}/virtualenv",
    wsgi_python_path => $caldance_app,
    require          => Package["mod_wsgi"],
  }
  class { 'apache::mod::authn_file': }
  class { 'apache::mod::authn_core': }
  class { 'apache::mod::authz_user': }
  class { 'apache::mod::auth_basic': }
  class { 'apache::mod::proxy': }
  apache::mod { 'proxy_uwsgi': }

  apache::vhost { $web_host:
    port                 => '443',
    docroot              => false,
    manage_docroot       => false,
    ssl                  => true,
    ssl_cert             => "/etc/letsencrypt/live/$web_host/cert.pem",
    ssl_key              => "/etc/letsencrypt/live/$web_host/privkey.pem",
    ssl_chain            => "/etc/letsencrypt/live/$web_host/chain.pem",
    require              => Letsencrypt::Certonly[$web_host],
    proxy_preserve_host  => true,
    proxy_pass           => [
      {
        path          => "/",
        url           => "unix:$socket|uwsgi://caldance-app/",
        reverse_urls  => [],
        no_proxy_uris => [ "/media/", "/static/" ],
      }
    ],
    directories          => [
      {
        path    => "$caldance_app/main_app",
        require => "all granted",
      },
      {
        path    => "$caldance_app/www/static",
        require => "all granted",
      },
      {
        path    => "$home/media",
        require => "all granted",
        options => ["-Indexes"],
      },
      {
        path           => "/",
        provider       => "location",
        require        => "valid-user",
        auth_type      => "Basic",
        auth_name      => "Authentification requise",
        auth_user_file => "$home/htpasswd",
      },
    ],
    aliases              => [
      {
        alias => "/static/",
        path => "$caldance_app/www/static/",
      },
      {
        alias => "/media/",
        path => "$home/media/",
      },
    ];
    default: *           => $::profile::apache::apache_vhost_default;
  }

  file { "/etc/systemd/system/caldance-app.service":
    mode    => "0644",
    owner   => "root",
    group   => "root",
    content => template("role/caldance/caldance-app.service.erb"),
    require => File["$caldance_app/app.ini"],
  } ->
  service { "caldance-app":
    ensure => "running",
    enable => true
  }

  cron::job { "list_outdated_pip_packages":
    ensure => absent
  }
}
