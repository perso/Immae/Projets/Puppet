class role::backup (
  String            $user,
  String            $group,
  String            $mailto,
  Optional[Array]   $backups = [],
  Optional[String]  $mountpoint = "/backup1",
  Optional[String]  $backup_script = "/usr/local/bin/backup.sh",
) {
  include "base_installation"

  include "profile::fstab"
  include "profile::mail"
  include "profile::tools"
  include "profile::xmr_stak"
  include "profile::known_hosts"
  include "profile::boinc"
  include "profile::monitoring"

  include "role::backup::postgresql"

  ensure_packages(["rsync"])

  ssh_keygen { $user:
    notify => Notify_refresh["notify-backup-sshkey-change"]
  }

  $hosts = $backups.map |$backup| { $backup["host"] }

  notify_refresh { "notify-backup-sshkey-change":
    message     => template("role/backup/ssh_key_changed.info.erb"),
    refreshonly => true
  }

  $hosts.each |$host| {
    notify_refresh { "notify-backup-sshhost-$host-changed":
      message     => template("role/backup/ssh_host_changed.info.erb"),
      refreshonly => true,
      subscribe   => Sshkey[$host],
    }
  }

  concat { $backup_script:
    ensure         => "present",
    ensure_newline => true,
    mode           => "0755",
  }

  cron::job { "backup":
    ensure  => present,
    command => $backup_script,
    user    => $user,
    minute  => 25,
    hour    => "3,15",
    require => Concat[$backup_script],
  }

  concat::fragment { "backup_head":
    target  => $backup_script,
    content => template("role/backup/backup_head.sh.erb"),
    order   => "01-50",
  }

  concat::fragment { "backup_tail":
    target  => $backup_script,
    content => template("role/backup/backup_tail.sh.erb"),
    order   => "99-50",
  }

  $backups.each |$infos| {
    $dirname = $infos["name"]
    $login = $infos["login"]
    $host = $infos["host"]
    $port = if $infos.dig("port") { $infos.dig("port") } else { 22 }
    $dest = "$login@$host"
    $base = "$mountpoint/$dirname"
    $nbr  = $infos["nbr"]
    $order_dirname = $infos["order"]

    file { $base:
      ensure  => "directory",
      owner   => $user,
      group   => $group,
      require => Mount[$mountpoint],
    } ->
    file { "$base/older":
      ensure  => "directory",
      owner   => $user,
      group   => $group,
    } ->
    file { "$base/rsync_output":
      ensure  => "directory",
      owner   => $user,
      group   => $group,
    }

    concat::fragment { "backup_${dirname}_head":
      target  => $backup_script,
      content => template("role/backup/backup_dirname_head.sh.erb"),
      order   => "$order_dirname-01",
    }

    concat::fragment { "backup_${dirname}_tail":
      target  => $backup_script,
      content => template("role/backup/backup_dirname_tail.sh.erb"),
      order   => "$order_dirname-99",
    }

    $infos["parts"].each |$part| {
      $local_folder = $part["local_folder"]
      $remote_folder = $part["remote_folder"]
      $exclude_from = $part["exclude_from"]
      $files_from = $part["files_from"]
      $args = $part["args"]
      $order_part = $part["order"]

      file { "$base/$local_folder":
        ensure  => "directory",
        owner   => $user,
        group   => $group,
        require => File[$base],
      }

      concat::fragment { "backup_${dirname}_${local_folder}":
        target  => $backup_script,
        content => template("role/backup/backup_dirname_part.sh.erb"),
        order   => "$order_dirname-$order_part",
      }
    }

    @profile::monitoring::local_service { "Last backup in $base is not too old":
      local => {
        check_command => "check_last_file_date!$base!14",
      }
    }
  }
}
