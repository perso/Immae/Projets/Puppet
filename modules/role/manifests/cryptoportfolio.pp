class role::cryptoportfolio (
  String            $user,
  String            $group,
  String            $home,
  Optional[String]  $env                 = "prod",
  Optional[String]  $webhook_url         = undef,
  String            $pg_user,
  String            $pg_db,
  Optional[String]  $pg_hostname          = "/run/postgresql",
  Optional[String]  $pg_port              = "5432",
  Optional[String]  $redis_host           = "/run/redis/redis.sock",
  Optional[String]  $smtp_login           = undef,
  Optional[String]  $smtp_password        = undef,
  Optional[String]  $smtp_host            = undef,
  Optional[String]  $mail_from            = undef,
  Optional[String]  $web_host             = undef,
  Optional[String]  $web_port             = "",
  Optional[Boolean] $web_ssl              = true,
  Optional[String]  $front_version        = undef,
  Optional[String]  $front_sha256         = undef,
  Optional[String]  $trader_version       = undef,
  Optional[String]  $trader_sha256        = undef,
) {
  include "base_installation"

  include "profile::tools"
  include "profile::postgresql"
  include "profile::apache"
  include "profile::xmr_stak"
  include "profile::boinc"
  include "profile::monitoring"

  contain "role::cryptoportfolio::postgresql"
  contain "role::cryptoportfolio::apache"

  contain "role::cryptoportfolio::notify"

  unless empty($trader_version) {
    if versioncmp($trader_version, "v1.3") >= 0 {
      include "profile::redis"
    }
    contain "role::cryptoportfolio::bot"
  }

  unless empty($front_version) {
    contain "role::cryptoportfolio::front"
  }
}
