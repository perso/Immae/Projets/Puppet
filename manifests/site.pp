node default {
  lookup('classes').each |$class_name, $class_hash| {
    if $class_hash == undef  or empty($class_hash) {
      include $class_name
    } else {
      class { $class_name:
        * => $class_hash
      }
    }

  }
}
