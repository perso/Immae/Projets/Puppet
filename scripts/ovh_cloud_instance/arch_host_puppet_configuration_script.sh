#!/bin/bash

git_branch="$1"
environment="$2"

MOUNTPOINT=/mnt

sudo cp /tmp/arch_puppet_configuration_script.sh "$MOUNTPOINT/root/"

sudo arch-chroot "$MOUNTPOINT" /root/arch_puppet_configuration_script.sh "$git_branch" "$environment"

sudo umount "$MOUNTPOINT"
