#!/bin/bash

set -e

git_branch="$1"
environment="$2"

# Randomizer
sudo haveged &
# /Randomizer

# Prepare an arch chroot
sudo pacman -Sy --noconfirm arch-install-scripts
# /Prepare an arch chroot

# Prepare device information
DEVICE=/dev/vdb1
MOUNTPOINT=/mnt

UUID=$(lsblk -rno UUID "$DEVICE")
PART="/dev/disk/by-uuid/$UUID"
# /Prepare device information

# Install very basic system (base git puppet)
# mkfs.ext4 -F -U "$UUID" "$DEVICE"
sudo mount "$DEVICE" /mnt

##### mkfs.ext4 would be better ####
for i in /mnt/*; do
  if [ "$i" = "/mnt/boot" ]; then
    # keep /boot/grub
    sudo rm -f $i/* || true
  else
    sudo rm -rf $i
  fi
done
##### / ####

sudo pacstrap -G /mnt base git puppet

echo "$PART / auto defaults 0 1" | sudo tee /mnt/etc/fstab
# /Install very basic system

# Install rest of system (via puppet)
sudo cp /tmp/arch_install_script.sh "$MOUNTPOINT/root/"
sudo cp /tmp/puppet_variables.json "$MOUNTPOINT/root/"

sudo arch-chroot "$MOUNTPOINT" /root/arch_install_script.sh "$git_branch" "$environment"
# /Install rest of system

