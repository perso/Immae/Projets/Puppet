#!/bin/bash

git_branch="$1"
environment="$2"
CODE_PATH="/etc/puppetlabs/code"

rm -rf $CODE_PATH

pacman-key --init
pacman-key --populate archlinux

git clone -b $git_branch https://git.immae.eu/perso/Immae/Projets/Puppet.git $CODE_PATH
cd $CODE_PATH
git submodule update --init

export FACTER_in_chroot="yes"
puppet apply --environment $environment --tags base_installation --modulepath $CODE_PATH/modules:$CODE_PATH/external_modules --test $CODE_PATH/manifests/site.pp
# The password seed requires puppet to be run twice
puppet apply --environment $environment --tags base_installation --modulepath $CODE_PATH/modules:$CODE_PATH/external_modules --test $CODE_PATH/manifests/site.pp

