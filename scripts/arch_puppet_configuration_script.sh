#!/bin/bash

git_branch="$1"
environment="$2"
CODE_PATH="/etc/puppetlabs/code"

export FACTER_in_chroot="yes"
puppet apply --environment $environment --tags base_installation --test $CODE_PATH/manifests/site.pp

