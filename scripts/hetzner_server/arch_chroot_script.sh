#!/bin/bash

pacman-key --init
pacman-key --populate archlinux

UUID=$(cat /device_uuid)
PART="/dev/disk/by-uuid/$UUID"
DEVICE=$(realpath "$PART")

mkfs.ext4 -F -U "$UUID" "$DEVICE"
mount "$DEVICE" /mnt

pacstrap -G /mnt base git puppet

echo "$PART / auto defaults 0 1" > /mnt/etc/fstab

umount /mnt

