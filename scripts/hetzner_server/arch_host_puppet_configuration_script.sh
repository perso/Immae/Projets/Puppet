#!/bin/bash

git_branch="$1"
environment="$2"

DEVICE="/dev/sda1"
MOUNTPOINT="/mnt"

cp /tmp/arch_puppet_configuration_script.sh "$MOUNTPOINT/root/"

/tmp/root.x86_64/bin/arch-chroot "$MOUNTPOINT" /root/arch_puppet_configuration_script.sh "$git_branch" "$environment"

umount "$MOUNTPOINT"
