#!/usr/bin/expect -f
set dest [lindex $argv 0]
set password [lindex $argv 1]
set git_branch [lindex $argv 2]
set environment [lindex $argv 3]
set script [lindex $argv 4]
set files [lrange $argv 4 end]

if {$password == "x"} {
  set ask_password 0
} else {
  set ask_password 1
}

set scriptname [file tail $script]

set sshopts [split "-o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o CheckHostIP=no"]

set timeout -1
spawn scp {*}$sshopts {*}$files $dest:/tmp
if {$ask_password} {
  expect "assword:"
  send "$password\n"
}
expect eof
spawn ssh {*}$sshopts $dest /tmp/$scriptname $git_branch $environment
if {$ask_password} {
  expect "assword:"
  send "$password\n"
}
expect eof
