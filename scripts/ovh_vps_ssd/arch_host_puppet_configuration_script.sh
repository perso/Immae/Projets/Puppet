#!/bin/bash

git_branch="$1"
environment="$2"

DEVICE_STR=$(cat /proc/mounts | grep "/dev/[sv]d.. /mnt/")
DEVICE=$(echo "$DEVICE_STR" | cut -d' ' -f1)
MOUNTPOINT=$(echo "$DEVICE_STR" | cut -d' ' -f2)

cp /tmp/arch_puppet_configuration_script.sh "$MOUNTPOINT/root/"

/tmp/root.x86_64/bin/arch-chroot "$MOUNTPOINT" /root/arch_puppet_configuration_script.sh "$git_branch" "$environment"

umount "$MOUNTPOINT"
