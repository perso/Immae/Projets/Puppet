#!/bin/bash

set -e

git_branch="$1"
environment="$2"

# Randomizer
apt-get update
apt-get install -y haveged
haveged &
# /Randomizer

# Prepare an arch chroot
cd /tmp

LATEST=$(curl -L https://mirrors.kernel.org/archlinux/iso/latest/sha1sums.txt | grep "bootstrap" | head -n1)
SHA1=$(echo "$LATEST" | cut -d' ' -f1)
NAME=$(echo "$LATEST" | cut -d' ' -f3)

curl -L -O "https://mirrors.kernel.org/archlinux/iso/latest/$NAME"

tar -xzf "$NAME"

echo 'Server = http://archlinux.mirrors.ovh.net/archlinux/$repo/os/$arch' > /tmp/root.x86_64/etc/pacman.d/mirrorlist
# /Prepare an arch chroot

# Prepare device information (not available in chroot)
DEVICE_STR=$(cat /proc/mounts | grep "/dev/[sv]d.. /mnt/")
DEVICE=$(echo "$DEVICE_STR" | cut -d' ' -f1)
MOUNTPOINT=$(echo "$DEVICE_STR" | cut -d' ' -f2)

umount "$DEVICE"
UUID=$(lsblk -rno UUID "$DEVICE")

echo "$UUID" > /tmp/root.x86_64/device_uuid
# /Prepare device information

# Install very basic system via chroot (base git puppet)
cp /tmp/arch_chroot_script.sh /tmp/root.x86_64/

/tmp/root.x86_64/bin/arch-chroot /tmp/root.x86_64/ /arch_chroot_script.sh
# /Install very basic system via chroot

# Mount and install rest of system (via puppet)
mount "$DEVICE"

cp /tmp/arch_install_script.sh "$MOUNTPOINT/root/"
cp /tmp/puppet_variables.json "$MOUNTPOINT/root/"

/tmp/root.x86_64/bin/arch-chroot "$MOUNTPOINT" /root/arch_install_script.sh "$git_branch" "$environment"
# /Mount and install rest of system

