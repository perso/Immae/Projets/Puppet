#!/bin/bash

usage() {
cat <<EOF
$(basename $0) [options]
  --help,-h               This help

  Please chose environment:
  --environment env       Environment to use for the install

  One of the following options is necessary:
  --instance-id id        Id of the cloud instance
  --vps-id id             Id of the vps
  --hetzner-id id         Id of the Hetzner server

  Optional arguments:
  --password password     Password of the host (only useful in case of no reboot and vps)
  --reinstall-first       Start with reinstalling the vps
  --host-user user        Use another user than the default one
  --no-reboot             Don't reboot
  --no-reboot-start       Don't reboot to rescue at the beginning
  --no-reboot-end         Don't reboot to normal at the end
  --git-branch branch     Use another puppet branch (default: master)
EOF
}

set -e

git_branch=master
host_user=""
password=""
T=""

while [ -n "$1" ]; do
  case "$1" in
    --instance-id)
      host_id="$2"
      if [ -z "$host_user" ]; then
        host_user="arch"
      fi
      if [ -z "$password" ]; then
        password="x"
      fi
      [ -n "$T" ] && usage && exit 1
      T="ovh_cloud_instance"
      shift
      ;;
    --vps-id)
      host_id="$2"
      if [ -z "$host_user" ]; then
        host_user="root"
      fi
      [ -n "$T" ] && usage && exit 1
      T="ovh_vps_ssd"
      shift
      ;;
    --hetzner-id)
      host_id="$2"
      if [ -z "$host_user" ]; then
        host_user="root"
      fi
      [ -n "$T" ] && usage && exit 1
      T="hetzner_server"
      shift
      ;;
    --password)
      password="$2"
      shift
      ;;
    --reinstall-first)
      reinstall_first=1
      ;;
    --host-user)
      host_user="$2"
      shift
      ;;
    --no-reboot)
      no_reboot=1
      ;;
    --no-reboot-start)
      no_reboot_start=1
      ;;
    --no-reboot-end)
      no_reboot_end=1
      ;;
    --git-branch)
      git_branch="$2"
      shift
      ;;
    --environment)
      environment="$2"
      shift
      ;;
    --help|-h)
      usage
      exit 0
      ;;
  esac

  shift
done

if [ -z "$T" -o -z "$host_id" -o -z "$environment" ]; then
  usage
  exit 1
fi

DIRECTORY=$(cd `dirname $0` && pwd)
PYTHON_DIRECTORY="$DIRECTORY/../python"
SCRIPTS="$DIRECTORY/../scripts"

if [ -n "$reinstall_first" ]; then
  echo "Réinstallation du système"
  python $PYTHON_DIRECTORY/reinstall_$T.py --use-current "$host_id"

  read -p "Appuyer sur une touche quand le serveur est prêt" ready
fi

if [ -z "$no_reboot" -a -z "$no_reboot_start" ]; then
  echo "Patienter le temps du reboot"
  python $PYTHON_DIRECTORY/reboot_$T.py --rescue "$host_id"

  read -p "Appuyer sur une touche quand l'instance a redémarré" ready
fi

if [ -z "$password" ]; then
  stty -echo
  read -p "Mot de passe reçu par e-mail : " password; echo
  stty echo
fi

ARCH_DIR=`mktemp -d`
ARCH_HOST_SCRIPT="$SCRIPTS/$T/arch_host_script.sh"
if [ -f "$SCRIPTS/$T/arch_chroot_script.sh" ]; then
  ARCH_CHROOT_SCRIPT="$SCRIPTS/$T/arch_chroot_script.sh"
else
  ARCH_CHROOT_SCRIPT=""
fi
ARCH_INSTALL_SCRIPT="$SCRIPTS/arch_install_script.sh"
ARCH_HOST_PUPPET_CONFIGURATION_SCRIPT="$SCRIPTS/$T/arch_host_puppet_configuration_script.sh"
ARCH_PUPPET_CONFIGURATION_SCRIPT="$SCRIPTS/arch_puppet_configuration_script.sh"
ARCH_PUPPET_INITIAL_CONFIGURATION="$ARCH_DIR/puppet_variables.json"

trap "rm -rf $ARCH_DIR" EXIT

#### Base installation stage
python $PYTHON_DIRECTORY/get_initial_configuration_$T.py $host_id > $ARCH_PUPPET_INITIAL_CONFIGURATION
host_address=$(python $PYTHON_DIRECTORY/get_initial_configuration_$T.py $host_id | jq -r '.ips.v4.ipAddress')

dest="$host_user@$host_address"
files="$ARCH_HOST_SCRIPT $ARCH_CHROOT_SCRIPT $ARCH_PUPPET_INITIAL_CONFIGURATION $ARCH_INSTALL_SCRIPT"

$SCRIPTS/send_and_run.tcl "$dest" "$password" "$git_branch" "$environment" $files

### Role specific stage
read -p "Press key when LDAP is configured" i

files="$ARCH_HOST_PUPPET_CONFIGURATION_SCRIPT $ARCH_PUPPET_CONFIGURATION_SCRIPT"

$SCRIPTS/send_and_run.tcl "$dest" "$password" "$git_branch" "$environment" $files

### Installation finished
if [ -z "$no_reboot" -a -z "$no_reboot_end" ]; then
  echo "Rebooting"
  python $PYTHON_DIRECTORY/reboot_$T.py --local "$host_id"
fi
