# Puppet configuration repository for immae.eu's services

This repository has the aim to help automate the installation of servers
planned for a specific task, with the help of Puppet. The host are
supposed to be listed in an LDAP-like database, which will contain the
necessary credentials, variable configuration and secrets for each
server.

## Structure

The repository is structured along Puppet modules (`modules/`
directory). Each machine has one or several `role`, which determine the
set of programs and configuration to install. Each role may be
standalone, or require a set of `profile`, which is seen as a
reusable component. (The structure is inspired from the tutorial at
[https://www.craigdunn.org/2012/05/239/](https://www.craigdunn.org/2012/05/239/) )


## TODO

- Complete documentation
- Add some monitoring:
  - modules/profile/manifests/postgresql/ssl.pp (check postgresql certificate)
  - modules/profile/manifests/postgresql/backup\_pgbouncer.pp (check pgbouncer works)
  - modules/profile/manifests/mail.pp (check e-mails are going through)
  - modules/profile/manifests/redis.pp (check redis is running)
    - LASTSAVE
- Add redis replication and dumps
- Restore backups for cryptoportfolio
- Ensure latest by default for packages
- try to do a mkfs.ext4 for cloud vps
